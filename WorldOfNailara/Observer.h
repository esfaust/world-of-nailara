#pragma once


#include <memory>

#include "Event.h"
#include "InputEvent.h"
#include "Location.h"

// A virtual class for denoting that a class wants to see events from an Observable
// They will need to define how they react to such an event, otherwise nothing happens
class Observer
{
public:
	// Notification call for input events
	virtual void onNotify(Event event, InputEvent input) {};
	virtual void onNotify(Event event, Location location) {};
	virtual void onNotify(Event event) {};
};