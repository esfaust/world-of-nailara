#include "TimeKeeper.h"

#include <algorithm>
#include <iostream>

void TimeKeeper::tick() {
	if (travelers.size() != 0) {
		Actor* a = travelers[0];
		
		int remainingAP = a->takeTurn();

		if (remainingAP < 0) {
			a->regenerateAP();
			std::rotate(travelers.begin(), travelers.begin() + 1, travelers.end());
		}

	}
}

void TimeKeeper::registerActor(Actor* a) {
	if (a == NULL) {
		std::cout << "Null actor.\n";
		return;
	}
	travelers.push_back(a);
}

void TimeKeeper::releaseActor(Actor* a) {
	travelers.erase(std::remove(travelers.begin(), travelers.end(), a), travelers.end());
}