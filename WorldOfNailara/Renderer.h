﻿#pragma once

#include <vector>
#include <algorithm>
#include <functional>

#include "Color.h"
#include "RenderItem.h"

#define MAX_QUEUE_ITEMS 1000

class Shader;
class Texture;
class SpriteSheet;
class Sprite;
class StringRenderer;
class Font;
class PrettyCharacter;




class Renderer
{
public:
	virtual void drawTag(std::string tag, int x, int y, std::string color = "white") =0;
	virtual void setLayer(int index) = 0;
	// Renders the UI and the active area
	virtual void render() = 0;
	virtual void clear() = 0;

    virtual void queueSpriteSheet(int x, int y, int z, int width, int height, SpriteSheet* sheet, int spriteIndex) = 0;
    virtual void queueSprite(int x, int y, int z, Sprite* sprite) = 0;
    virtual void queueSpriteResized(int x, int y, int z, int width, int height, Sprite* sprite) = 0;
    virtual void queueQuad(int x, int y, int z, int width, int height, Shader* shader, Texture* texture, bool tiling = false) = 0;
    virtual void queueQuadBordered(int x, int y, int z, int width, int height, Shader* shader, Texture* texture, Texture* border) = 0;
    virtual void queueText(int x, int y, int z, std::string text, Font* font, float scale, Color color, Shader* shader) = 0;
	virtual void queueCharacter(int x, int y, int z, char ch, Font* font, float scale, Color color, Shader* shader) = 0;

	virtual void queuePrettyCharacter(int x, int y, int z, PrettyCharacter ch) =0;

    virtual void drawText(int x, int y, int z, std::string text, Font* font,  float scale, Color color = Color()) = 0;
	virtual void drawCharacter(int x, int y, int z, char ch, Font* font, float scale, Color color = Color()) = 0;

    int getHeight() { return height_; }
    int getWidth() { return width_; }

    virtual StringRenderer* getStringRenderer() =0;


    // Draw a quad and automatically tile the texture
    virtual void drawQuadTiling(int x, int y, int z, int width, int height, Shader* shader, Texture* texture, float rotation = 0.0f) =0;
    // Draw a quad and resize the texture across the full quad
    virtual void drawQuadFull(int x, int y, int z, int width, int height, Shader* shader, Texture* texture, float rotation = 0.0f) = 0;
    // Draw a quad using the given texture coordinates
    virtual void drawQuad(int x, int y, int z, int width, int height, Shader* shader, Texture* texture, float textureCoordinates[8], float rotation = 0.0f, Color color = Color(1.0f, 1.0f, 1.0f)) = 0;

    // REMOVE THIS
    // Draw a quad bordered by another textured quad
    virtual void drawQuadBordered(int x, int y, int z, int width, int height, Shader* shader, Texture* background, Texture* border, float rotation = 0.0f) = 0;



	// New queueing style functions below

	void add(RenderItem* item);

	void destroy(RenderItem* data);

	static bool comp(RenderItem *lhs, RenderItem *rhs);

protected:
	int important = 1000;

	RenderItem* itemQueue_[MAX_QUEUE_ITEMS];
	RenderItem* sortedQueue_[MAX_QUEUE_ITEMS];
	bool filledQueue_;
	int queueLowestOpen_ = 0;
	int queueHighestUsed_ = -1;

    int width_ = 0;
    int height_ = 0;
};


// This is a null display used in case the locator is never provided a display to use
class NullRenderer : public Renderer
{
public:
	NullRenderer() {};
};