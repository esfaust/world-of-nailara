#include "Body.h"
#include "Form.h"
#include "Bodypart.h"

#define MAX_TAGS 10

std::string FormDescriptor::applyAttributesToTemplate(std::string descTemplate) {
    std::string desc = descTemplate;
    std::string newDesc = descTemplate;
    int start = 0, end = 0, pos = 0;
    for (auto ch : desc) {
        if (ch == '{') {
            start = pos;
        }
        else if (ch == '}') {
            end = pos;
            std::string tag = desc.substr(start + 1, end - start - 1);
            std::string replacement = "{" + tag + "}";
            newDesc.replace(newDesc.find(replacement), replacement.size(), parent_->attr<std::string>(tag));
        }
        pos++;
    }
    return newDesc;
}


std::string FormDescriptor::getDescription(bool longDescription) {
    if (longDescription && !descriptionGenerated_) {
        std::string fulldesc = "";
        
        if (parent_->attrExists("height")) {
            int height = parent_->attr<int>("height");
            int feet = int(height / 12);
            int inches = height % 12;
            std::string strHeight = std::to_string(feet) + "\'" + std::to_string(inches) + "\"";

            if (parent_->attrExists("raceName")) {
                fulldesc = "They are a " + strHeight + " tall " + parent_->attr<std::string>("raceName") + ".";
            }
            else {
                fulldesc = "They are " + strHeight + " tall.";
            }
        }
        // Get bodypart descriptions
        BodypartConnection base = layout_->getBase();
        std::vector<BodypartConnection> children = base.getChildren();


        for (auto child : children) {
            std::string childDesc = child.getDescriptionTemplate();
            if (childDesc != "") {
                if (fulldesc == "") {
                    fulldesc = child.getDescriptionTemplate();
                }
                else {
                    fulldesc = fulldesc + " " + child.getDescriptionTemplate();
                }
            }
        }

        longDescription_ = applyAttributesToTemplate(fulldesc);
        return longDescription_;
    }
    else if (!longDescription) {
        return longDescription_;
    }
    return shortDescription_;
}

BodypartConnection Form::getBase() {
    return base_;
}