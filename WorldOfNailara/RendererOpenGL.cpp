﻿#include "RendererOpenGL.h"

#include <algorithm>

// Other includes
#include "Locator.h"
#include "Shader.h"
#include "Texture.h"
#include "Sprite.h"
#include "SpriteSheet.h"
#include "Logger.h"
#include "Library.h"
#include "InputGL.h"
#include "StringRenderer.h"
#include "Loader.h"
#include "Color.h"

#include <ft2build.h>
#include FT_FREETYPE_H

#define TEST_GRAPHICS false

const GLuint WIDTH = 1920, HEIGHT = 1080;

RendererOpenGL::RendererOpenGL(std::string windowName, int height, int width)
{
	init();
    stringRenderer_ = new StringRenderer(window_, projection_);
    width_ = WIDTH;
    height_ = HEIGHT;
}

RendererOpenGL::~RendererOpenGL()
{
}

void RendererOpenGL::init() {
	// Init GLFW
	glfwInit();
	// Set all the required options for GLFW
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    // Create a GLFWwindow object that we can use for GLFW's functions
    window_ = glfwCreateWindow(WIDTH, HEIGHT, "World of Nailara", nullptr, nullptr);
    glfwMakeContextCurrent(window_);

    // Set this to true so GLEW knows to use a modern approach to retrieving function pointers and extensions
    glewExperimental = GL_TRUE;
    // Initialize GLEW to setup the OpenGL Function pointers
    glewInit();

    glEnable(GL_CULL_FACE);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glEnable(GL_DEPTH_TEST);
    glDepthRange(-1.0f, 1.0f);
    glDepthFunc(GL_LEQUAL);
    glClearDepth(1.0f);

    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    // Define the viewport dimensions
    glViewport(0, 0, WIDTH, HEIGHT);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, WIDTH, HEIGHT, 0, -1, 1);
    projection_ = glm::ortho(0.0f, static_cast<GLfloat>(WIDTH), static_cast<GLfloat>(HEIGHT), 0.0f, -1.0f, 1.0f);
    glMatrixMode(GL_MODELVIEW);

    glGenVertexArrays(1, &quadVAO_);
    glGenBuffers(1, &quadVBO_);
    glGenBuffers(1, &quadEBO_);

    qIndices_ = {
        0, 1, 3, // First Triangle
        1, 2, 3,  // Second Triangle
    };

    glBindVertexArray(quadVAO_);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, quadEBO_);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * qIndices_.size(), qIndices_.data(), GL_STATIC_DRAW);

    glfwSwapInterval(0);

    Locator::provide(new InputGL(window_)); //Provide new input listener on OpenGL window
}

void RendererOpenGL::clear() {
    //queue_.clear();
    //queueMap_.clear();
    rqueue_.clear();
}

#include "Common.h"
#include "Display.h"
int frameCounter = 0;
double lfps = 0;
double lastTick = 0;
double lastUpdate = 0;
void RendererOpenGL::fps() {
    static Font* font = Locator::getLibrary<Font*>()->get<Font*>("arial-bold");
    static Shader* shader = Locator::getLibrary<Shader*>()->get<Shader*>("basic");
    font->getTexture()->bind(0);
    shader->Use();
   
    double currentTime = glfwGetTime();
    double diff = 1 / (currentTime - lastTick);
    lastTick = currentTime;

    if (currentTime - lastUpdate >= 0.5f) {
        lfps = diff;
        lastUpdate = currentTime;
    }

    stringRenderer_->drawText(0, 0, 99, std::to_string(int(lfps)), font,  1.0f);
    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
}

void RendererOpenGL::drawText(int x, int y, int z, std::string text, Font* font, float scale, Color color) {
    stringRenderer_->drawText(x, y, z, text, font, scale, color);
}

void RendererOpenGL::drawCharacter(int x, int y, int z, char ch, Font* font, float scale, Color color) {
	stringRenderer_->drawChar(x, y, z, ch, font, scale, color);
}

#include <algorithm>
#include "RenderItem.h"

void RendererOpenGL::render() {
	Locator::getLogger();
    std::sort(rqueue_.begin(), rqueue_.end());




    if (!glfwWindowShouldClose(window_)) {
        // Check if any events have been activiated (key pressed, mouse moved etc.) and call corresponding response functions
        //glfwPollEvents();

        // Clear the colorbuffer
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		if (TEST_GRAPHICS) {
             test();
		}
		else {
			// Background quad
            Shader* currents = NULL;
            Texture* currentt = NULL;

			Locator::getLibrary<Shader*>()->get<Shader*>("basic")->Use();

			for (auto item : itemQueue_) {
				if (item == NULL) {
					break;
				}
				else {
					if (currents != item->getShader()) {
						currents = item->getShader();
						item->getShader()->Use();
					}
					
					if (currentt != item->getTexture()) {
						currentt = item->getTexture();
						currentt->bind();
					}
					item->draw();
				}

			}

			Locator::getLogger();

            for (auto item : rqueue_) {
                if (currents != item.shader_) {
                    currents = item.shader_;
                    item.shader_->Use();
                }
                if (currentt != item.texture_) {
                    currentt = item.texture_;
                    currentt->bind();
                }
                item.draw(this);
            }

            //drawQuadFull(0, 0, 0, 512, 512, debugs, debugt); // Test upscaling a texture
			Locator::getLibrary<Texture*>()->get<Texture*>("borderh")->bind();
			//drawQuadFull(300, 300, 0, 200, 200, Locator::getLibraryShader()->get("basic"), Locator::getLibraryTexture()->get("borderh"));

			static Font* font = Locator::getLibrary<Font*>()->get<Font*>("arial-bold");
            Locator::getLibrary<Shader*>()->get<Shader*>("basic")->Use();
			font->getTexture()->bind(0);

			fps();
		}

        // Swap the screen buffers
        glfwSwapBuffers(window_);
    }
    else {
        // Properly de-allocate all resources once they've outlived their purpose
        glDeleteVertexArrays(1, &quadVAO_);
        glDeleteBuffers(1, &quadVBO_);
        glDeleteBuffers(1, &quadEBO_);
        // Terminate GLFW, clearing any resources allocated by GLFW.
        glfwTerminate();
    }
}



void RendererOpenGL::drawQuadFull(int x, int y, int z, int width, int height, Shader* shader, Texture* texture, float rotation) {
    drawQuad(x, y, z, width, height, shader, texture, fullQuadDraw_, rotation);
}

void RendererOpenGL::drawQuadTiling(int x, int y, int z, int width, int height, Shader* shader, Texture* texture, float rotation) {
    float widthTiling = float(width / float(texture->getWidth()));
    float heightTiling = float(height / float(texture->getHeight()));

    float tiling[8] = {
        1.0f - widthTiling, 1.0f - heightTiling
        , 1.0f - widthTiling, 1.0f
        , 1.0f, 1.0f
        , 1.0f, 1.0f - heightTiling
    };
    drawQuad(x, y, z, width, height, shader, texture, tiling, rotation);
}


void RendererOpenGL::drawQuad(int x, int y, int z, int width, int height, Shader* shader, Texture* texture, float tc[8], float rotation, Color color) {
    glm::mat4 model;

    // Translate into place and onto correct layer
    model = glm::translate(model, glm::vec3(x, y, (float(z) / 100.0f)));

    // Fix non square rotation
    float diff = 0.0f;
    float rot = rotation;

    model = glm::translate(model, glm::vec3(width / 2, height / 2, 0.0f));
    model = glm::rotate(model, glm::radians(rot), glm::vec3(0.0f, 0.0f, 1.0f));
    model = glm::translate(model, glm::vec3(((-width / 2)), (-height / 2), 0.0f));
    model = glm::scale(model, glm::vec3(width, height, 1.0f));

    glUniformMatrix4fv(glGetUniformLocation(shader->Program, "model"), 1, GL_FALSE, glm::value_ptr(model));
    glUniformMatrix4fv(glGetUniformLocation(shader->Program, "projection"), 1, GL_FALSE, glm::value_ptr(projection_));

    glUniform2fv(glGetUniformLocation(shader->Program, "textureCoordinates"), 4, tc);

	glUniform1f(glGetUniformLocation(shader->Program, "time"), float(Common::time()));
	glUniform1f(glGetUniformLocation(shader->Program, "centerX"), float(x));

    glUniform4f(glGetUniformLocation(shader->Program, "color"), color.r_, color.g_, color.b_, color.alpha_);
    glUniform1i(glGetUniformLocation(shader->Program, "fragTexture"), 0);

    glBindBuffer(GL_ARRAY_BUFFER, quadVBO_);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 12, vertices_, GL_STATIC_DRAW);

    //// Position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
    glEnableVertexAttribArray(0);

    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);


	if (shader == Locator::getLibrary<Shader*>()->get<Shader*>("text")) {
		//Locator::getLogger()->log(SEV_INFO, "shader");
	}
}

void RendererOpenGL::drawQuadBordered(int x, int y, int z, int width, int height, Shader* shader, Texture* background, Texture* border, float rotation) {
    
    //Background
    drawQuadTiling(x + border->getWidth(), y + border->getHeight(), z, width- border->getWidth(), height- border->getHeight(), shader, background);

    border->bind();

    // Vertical
    drawQuadTiling(x, y, z+1, border->getWidth(), height, shader, border); // Right
    drawQuadTiling(x + width - border->getWidth(), y, z + 1, border->getWidth(), height, shader, border); // Left

    // Horizontal
    drawQuadTiling(x, y, z + 1, width, border->getHeight(), shader, border);
    drawQuadTiling(x, y + height - border->getHeight(), z + 1, width, border->getHeight(), shader, border);

    background->bind();
}



void RendererOpenGL::queueSpriteSheet(int x, int y, int z, int width, int height, SpriteSheet* sheet, int spriteIndex) {
    //queueMap_[sheet->getShader()][sheet->getTexture()].push_back(RenderQueueItem(x, y, z, width, height, sheet, spriteIndex));
    //queueMapZOrdered_[z][sheet->getShader()][sheet->getTexture()].push_back(RenderQueueItem(x, y, z, width, height, sheet, spriteIndex));
    rqueue_.push_back(RenderQueueItem(x, y, z, width, height, sheet, spriteIndex));
}

void RendererOpenGL::queueSprite(int x, int y, int z, Sprite* sprite) {
    sprite->queue(x, y, z);
}

void RendererOpenGL::queueSpriteResized(int x, int y, int z, int width, int height, Sprite* sprite) {
    sprite->queue(x, y, z, width, height);
}

void RendererOpenGL::queueQuad(int x, int y, int z, int width, int height, Shader* shader, Texture* texture, bool tiling) {
    //queueMap_[shader][texture].push_back(RenderQueueItem(x, y, z, width, height, shader, texture, tiling));
    rqueue_.push_back(RenderQueueItem(x, y, z, width, height, shader, texture, tiling));
}

void RendererOpenGL::queueQuadBordered(int x, int y, int z, int width, int height, Shader* shader, Texture* texture, Texture* border) {
    //queueMap_[shader][texture].push_back(RenderQueueItem(x, y, z, width, height, shader, texture, border));
    //queueMapZOrdered_[z][shader][texture].push_back(RenderQueueItem(x, y, z, width, height, shader, texture, border));
    rqueue_.push_back(RenderQueueItem(x, y, z, width, height, shader, texture, border));
}

void RendererOpenGL::queueCharacter(int x, int y, int z, char ch, Font* font, float scale, Color color, Shader* shader) {
	//queueMap_[shader][font->getTexture()].push_back(RenderQueueItem(x, y, z, text, font, scale, color, shader));
	//queueMapZOrdered_[z][shader][font->getTexture()].push_back(RenderQueueItem(x, y, z, text, font, scale, color, shader));
	rqueue_.push_back(RenderQueueItem(x, y, z, ch, font, scale, color, shader));
}

void RendererOpenGL::queueText(int x, int y, int z, std::string text, Font* font, float scale, Color color, Shader* shader) {
    //queueMap_[shader][font->getTexture()].push_back(RenderQueueItem(x, y, z, text, font, scale, color, shader));
    //queueMapZOrdered_[z][shader][font->getTexture()].push_back(RenderQueueItem(x, y, z, text, font, scale, color, shader));
    rqueue_.push_back(RenderQueueItem(x, y, z, text, font, scale, color, shader));
}



StringRenderer* RendererOpenGL::getStringRenderer() {
    return stringRenderer_;
}

void RendererOpenGL::testQueue() {
    
}

void RendererOpenGL::test() {
    clear();
    testQueue();

    Texture* debugt = Locator::getLibrary<Texture*>()->get<Texture*>("debug");
    Texture* errort = Locator::getLibrary<Texture*>()->get<Texture*>("error");
    Shader* debugs = Locator::getLibrary<Shader*>()->get<Shader*>("basic");

    debugt->bind();
    debugs->Use();

    drawQuadFull(0, 0, 0, 512, 512, debugs, debugt); // Test upscaling a texture
    drawQuadFull(512, 0, 0, 256, 256, debugs, debugt); // Test displaying a full texture
    drawQuadFull(512, 256, 0, 128, 128, debugs, debugt); // Test downscaling a texture

    // Test drawing from sprites and spritesheets
    Locator::getLibrary<Sprite*>()->get<Sprite*>("debug-sprite0")->draw(768, 0, 0);
    Locator::getLibrary<SpriteSheet*>()->get<SpriteSheet*>("debug-sheet")->draw(832, 0, 0, 4);
    Locator::getLibrary<SpriteSheet*>()->get<SpriteSheet*>("debug-sheet")->draw(896, 0, 0, 8);

    // Check z layering and sprite sheets
    Locator::getLibrary<SpriteSheet*>()->get<SpriteSheet*>("debug-sheet")->draw(768 + 10, 64 + 10, 5, 0);
    Locator::getLibrary<SpriteSheet*>()->get<SpriteSheet*>("debug-sheet")->draw(784 + 10, 96 + 10, 4, 1);
    Locator::getLibrary<SpriteSheet*>()->get<SpriteSheet*>("debug-sheet")->draw(800 + 10, 128 + 10, 3, 2);
    Locator::getLibrary<SpriteSheet*>()->get<SpriteSheet*>("debug-sheet")->draw(816 + 10, 160 + 10, 2, 3);
    Locator::getLibrary<SpriteSheet*>()->get<SpriteSheet*>("debug-sheet")->draw(832 + 10, 192 + 10, 1, 4);

    // Test tiling
    drawQuadTiling(0, 522, 0, 512, 64, debugs, debugt); // Larger X
    drawQuadTiling(0, 596, 0, 256, 64, debugs, debugt); // Equal X
    drawQuadTiling(0, 670, 0, 128, 64, debugs, debugt); // Smaller X

    drawQuadTiling(266, 596, 0, 128, 64, debugs, debugt, 90.0f); // Smaller X

    drawQuadBordered(512, 512, 1, 256, 256, debugs, debugt, errort);

    errort->bind();
    // Test rotation of a full quad
    drawQuadFull(648, 266, 0, 32, 32, debugs, errort);
    drawQuadFull(685, 266, 0, 32, 32, debugs, errort, 45);
    drawQuadFull(722, 266, 0, 32, 32, debugs, errort, 90);
    drawQuadFull(722, 303, 0, 32, 32, debugs, errort, 135);
    drawQuadFull(722, 340, 0, 32, 32, debugs, errort, 180);
    drawQuadFull(685, 340, 0, 32, 32, debugs, errort, 225);
    drawQuadFull(648, 340, 0, 32, 32, debugs, errort, 270);
    drawQuadFull(648, 303, 0, 32, 32, debugs, errort, 315);

    //Test queuing
    queueText(10, 744, 1, "White Normal", Locator::getLibrary<Font*>()->get<Font*>("arial-bold"), 1.0f, Color(1.0f, 1.0f, 1.0f), Locator::getLibrary<Shader*>()->get<Shader*>("text"));
    queueText(10, 788, 2, "Red Small", Locator::getLibrary<Font*>()->get<Font*>("arial-bold"), 0.5f, Color(1.0f, 0.0f, 0.0f), Locator::getLibrary<Shader*>()->get<Shader*>("text"));
    queueText(10, 788, 3, "Alpha Large", Locator::getLibrary<Font*>()->get<Font*>("arial-bold"), 1.5f, Color(1.0f, 0.0f, 1.0f, 0.5f), Locator::getLibrary<Shader*>()->get<Shader*>("text"));
    queueQuad(500, 788, 0, 50, 50, Locator::getLibrary<Shader*>()->get<Shader*>("basic"), Locator::getLibrary<Texture*>()->get<Texture*>("debug"));
    queueQuadBordered(550, 788, 0, 150, 150, Locator::getLibrary<Shader*>()->get<Shader*>("basic"), Locator::getLibrary<Texture*>()->get<Texture*>("debug"), Locator::getLibrary<Texture*>()->get<Texture*>("error"));
    queueQuad(700, 788, 0, 50, 50, Locator::getLibrary<Shader*>()->get<Shader*>("basic"), Locator::getLibrary<Texture*>()->get<Texture*>("debug"), true); // Tiling
    queueSpriteResized(960, 0, 0, 128, 128, Locator::getLibrary<Sprite*>()->get<Sprite*>("kona"));
    queueSprite(960, 0, 1, Locator::getLibrary<Sprite*>()->get<Sprite*>("kona"));
    queueSpriteSheet(896, 64, 0, 64, 64, Locator::getLibrary<SpriteSheet*>()->get<SpriteSheet*>("debug-sheet"), 12);

    for (auto& skv : queueMap_) {
        skv.first->Use();
        for (auto& tkv : skv.second) {
            tkv.first->bind(0);
            for (auto item : tkv.second) {
                item.draw(this);
            }
        }
    }
}

#include "PrettyCharacter.h"
void RendererOpenGL::queuePrettyCharacter(int x, int y, int z, PrettyCharacter ch) {

}