#pragma once

class Display;
class Area;

/*
Singleton engine class, which handles running the game update calls, and the initial setup when the game is booted.
*/
class Engine
{
public:
	enum EngineStatus
	{
		NEW,
		STARTUP,
		RUNTIME,
		SAVING,
		DESTRUCTING
	};

	//Only one engine instance sho
	static Engine& getInstance()
	{
		static Engine instance;
		return instance;
	}
	~Engine();

	// Initializes the game engine, creating loggers, display, and other required setup
	void init();

	// Kicks off the initial interactable game content
	void start();
	// Does another frame of updating
	void update();

	// Calls the inputs check to alert observers to new input
	void checkInput();
private:
	// Singletons should not have public constructors and setters
	Engine() { };
	Engine(Engine const&) { };
	void operator=(Engine const&) { };

	EngineStatus status_ = EngineStatus::NEW;

	// The display that will be used to display game content. Is actually located in the Locator class
	Display* display_;
	// The active zone to display
	Area* activeZone_;
};

