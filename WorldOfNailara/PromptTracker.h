#pragma once

#include <vector>

#include "PromptWindow.h"

class PromptTracker
{
public:
	PromptTracker();
	~PromptTracker();

	void addPrompt(PromptWindow *promptWindow);
	void removePrompt(PromptWindow *promptWindow);

	void draw();
	bool isEmpty();
	PromptWindow* getActivePrompt();

private:
	std::vector<PromptWindow*> prompts_;
};