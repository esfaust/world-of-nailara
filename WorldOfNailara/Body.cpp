#include "Body.h"
#include <string>

#include "Drawable.h"


Body::Body(Form* form) : form_(form), descriptor_(FormDescriptor(form, this)) {
	visual_ = new DrawableBody(this, Locator::getLibrary<Sprite*>()->get<Sprite*>("player"));
}

std::string Body::getDescription(bool longDescription) {
	return descriptor_.getDescription(longDescription);
}