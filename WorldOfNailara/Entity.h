#pragma once

#include <stdint.h>
#include <memory>
#include <boost\variant.hpp>
#include <unordered_map>

#include "BlockingFlags.h"
#include "Observable.h"
#include "Location.h"
#include "Logger.h"
#include "GraphicalLayering.h"

class Sprite;

// This class is used to represent physical things - it is the base class for things like actors, as well as items and furniture
// An entity can be observed for events like destruction, death, or movement
class Entity : std::enable_shared_from_this<Entity>, public Observable
{
public:
	Entity(std::string name, Sprite* ap, Location l);
	Entity(std::string name, Sprite* ap, Location l, Blocking::Set blocking);
	~Entity();

	// Displays the entity on the screen
	virtual void render(int x, int y, int z = GraphicalLayering::LAYER_ACTORS);
	// Updates if the entity has any ongoing effects like being on fire
	// Rarely used by items, and actor will overload this
	virtual void update();

	// Returns the location of the entity
	Location getLocation() { return loc_; }
	// Sets what this entity blocks the movement of, if anything
	void setBlocking(Blocking::Set blocksMovement) { blocking_ = blocksMovement; }

	// Shifts the entity by a number of spaces, physically moving there
	void shift(int dx, int dy);
	// Immediately teleports the entity to a new location
	void teleport(Location location);

	template<typename T> void attr(std::string key, T value);
	template<typename T> T attr(std::string key) {
		try {
			if (attributes_.count(key) != 0) {
				return boost::get<T>(attributes_[key]);
			}
			else {
				return T();
			}
		} catch (boost::bad_get&) {
			Locator::getLogger()->log(SEV_INFO, "Invalid attribute requested from entity?");
			return T();
		}
	}

protected:
	std::unordered_map<std::string, boost::variant<std::string>> attributes_;
	std::string name = "undefined";
	// The appearance to render for this entity
    Sprite* appearance_;
	// The location of the entity, initially in the void
	Location loc_ = Location(NULL, 0, 0);
	// The unique ID of the entity
	uint32_t id_;
	// What the entity blocks the movement of, if anything
	Blocking::Set blocking_ = Blocking::S_Creature;
};

