#include "TargetingSelf.h"

#include "Locator.h"
#include "Logger.h"
#include "Entity.h"

TargetingSelf::TargetingSelf(Entity* caller) : caller_(caller) {
}

TargetingSelf::~TargetingSelf() {
}

void TargetingSelf::target() {
	return;
}

bool TargetingSelf::hasValidTargets() {
	return true;
}

void TargetingSelf::applyEffect(Effect* effect) {
	try {
		effect->apply(caller_);
	}
	catch (Exception e) {
		typeid(effect).raw_name();
		Locator::getLogger()->log(SEV_INFO, std::string(typeid(this).name()) + " failed to apply effect '" + std::string(typeid(effect).name()));
	}
}