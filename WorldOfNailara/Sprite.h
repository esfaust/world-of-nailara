#pragma once

#include <string>

#include "Drawable.h"

class Texture;
class Shader;
class Renderer;
class SpriteSheet;

class Sprite : public Drawable
{
public:
    Sprite(SpriteSheet* sheet, int index);
	Sprite();
	~Sprite();

    void queue(int posX, int posY, int z);
    void queue(int posX, int posY, int z, int width, int height);
	void draw(int x, int y, int z);
    void draw(int x, int y, int z, int width, int height);

    int getHeight();
    int getWidth();
	Texture* getTexture();

private:
    SpriteSheet* sheet_;
    int index_ = 0;
};


