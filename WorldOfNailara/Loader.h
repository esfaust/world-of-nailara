#pragma once
class Loader
{
public:
    static void loadGraphics();
    static void loadFonts();
	static void loadBodies();
};

