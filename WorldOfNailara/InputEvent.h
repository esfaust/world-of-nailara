#pragma once

// This struct is used to store information about an input event
// I wanted to put it in Input.h, but dependencies were being rude...
struct InputEvent {
public:
	int key = 0;
    int action = 0;
	bool ctrl = false;
	bool alt = false;
	bool shift = false;
	bool lmb = false;
	bool rmb = false;
	bool mmb = false;
	int mx = -1;
	int my = -1;
	int mpx = -1;
	int mpy = -1;
};