#include "StringRenderer.h"

#include "Shader.h"
#include "Locator.h"
#include "Logger.h"

#include <algorithm>

#include <ft2build.h>
#include FT_FREETYPE_H

#include "Font.h"
#include "Display.h"
#include "Renderer.h"
#include "Library.h"

StringRenderer::StringRenderer(GLFWwindow* window, glm::mat4 projection) : window_(window), projection_(projection) {

}

Character StringRenderer::getCharacter(char c) {
    return characters_[c];
}

void StringRenderer::drawText(int startX, int startY, int z, std::string text, Font* font, float scale, Color color) {
    font->getTexture()->bind(0);
    int x = startX;
    std::string::const_iterator c;
    for (c = text.begin(); c != text.end(); c++)
    {
        Character ch = font->character(*c);
        drawCharacter(ch, x + (ch.xOffset_ * scale), startY + (ch.yOffset_ * scale), z, scale, color);
        x += ch.advance_ * scale;
    }
}

void StringRenderer::drawChar(int x, int y, int z, char ch, Font* font, float scale, Color color) {
    font->getTexture()->bind(0);
	Character character = font->character(ch);
	drawCharacter(character, x + (character.xOffset_ * scale), y + (character.yOffset_ * scale), z, scale, color);
}

void StringRenderer::drawCharacter(Character ch, int x, int y, int z, float scale, Color color) {
    float info[8] = {
        1.0f - (ch.tx + ch.tw), 1.0f - (ch.ty + ch.th)    //A
        , 1.0f - (ch.tx + ch.tw), 1.0f - (ch.ty)        //B
        , 1.0f - (ch.tx), 1.0f - (ch.ty)              //C
        , 1.0f - (ch.tx), 1.0f - (ch.ty + ch.th)        //D
    };
    Locator::getDisplay()->getRenderer()->drawQuad(x, y, z, ch.size_.x * scale, ch.size_.y * scale, Locator::getLibrary<Shader*>()->get<Shader*>("text"), Locator::getLibrary<Texture*>()->get<Texture*>("arial-bold"), info, 0.0f, color);
}