#include "PromptTracker.h"

#include <algorithm>
#include "Locator.h"
#include "Display.h"

PromptTracker::PromptTracker() { }

PromptTracker::~PromptTracker() {
	prompts_.clear();
}


void PromptTracker::draw() {
	Locator::getDisplay()->setLayer(100);
	for (auto p : prompts_) {
		p->draw();
	}
}

void PromptTracker::addPrompt(PromptWindow *pw)
{
	prompts_.push_back(pw);
}

void PromptTracker::removePrompt(PromptWindow *pw)
{
	prompts_.erase(std::remove(prompts_.begin(), prompts_.end(), pw), prompts_.end());
}

bool PromptTracker::isEmpty() {
	return prompts_.empty();
}

PromptWindow* PromptTracker::getActivePrompt() {
	PromptWindow* pw = prompts_[prompts_.size() - 1];
	return pw;
}