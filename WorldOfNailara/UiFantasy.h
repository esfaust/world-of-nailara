#pragma once
#include "UiBase.h"
class UiFantasy :
	public UiBase
{
public:
	UiFantasy(Renderer* renderer, ViewPort *view, bool asciiMode=false) : UiBase(renderer, view, asciiMode) {}

	void draw();
};

