#pragma once

#include "Observer.h"
#include "Observable.h"

class Area;

// This class is used for scrolling cameras and determining what is in the renderable area
class FocalPoint : public Observer, public Observable
{
public:
	FocalPoint() {};
	FocalPoint(int x, int y);
	~FocalPoint() {};

	// The coordinates of the 'camera' point to focus on
	int x_=0, y_=0;
	int lowX_=0, lowY_=0;
	int highX_=0, highY_=0;
	int padX_=0, padY_=0;

    float panningX = 0.0f, panningY = 0.0f;

	void setTracking(Observable* target);

	void onNotify(Event event, Location location);

private:
	Observable* trackingTarget_;
};