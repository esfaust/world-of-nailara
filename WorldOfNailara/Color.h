#pragma once

// Class to store RGBA values
class Color
{
public:
    Color() {};
    Color(float r, float g, float b, float alpha = 1.0f) : r_(r), g_(g), b_(b), alpha_(alpha) {};

    float r_ = 1.0f; // Red
    float g_ = 1.0f; // Green
    float b_ = 1.0f; // Blue
    float alpha_ = 1.0f; // Alpha
};

