#pragma once

#include <string>

#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/GLU.h>

// GLFW
#include <GLFW/glfw3.h>
#include <SOIL.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Texture.h"

struct Character {
	char char_;
    glm::ivec2 size_;
    glm::ivec2 bearing_;
    GLuint advance_;
    float x_ = 0.0f, y_ = 0.0f;
    float xOffset_, yOffset_;
    unsigned short page_;
    float tx, ty, tw, th;

    Character() : x_(0), y_(0), xOffset_(0), yOffset_(0), advance_(0), page_(0) {
        size_.x = 0;
        size_.y = 0;
        bearing_.x = 0;
        bearing_.y = 0;
		char_ = ' ';
    }
};

struct Charset
{
    unsigned short lineHeight_ = 0;
    unsigned short base_ = 0;
    unsigned short width_ = 0, height_ = 0;
    unsigned short pages_ = 0;
    Character Chars[256];
};

class Font {
public:
    Font() {};
    Font(std::string filename);
    Character character(int ch);
    void loadTexture();
    ~Font();

    Texture* getTexture() {
        return texture_;
    }

private:
    Charset charsetDesc_;
    GLuint textureID_;
    Texture* texture_;
};