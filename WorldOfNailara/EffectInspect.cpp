#include "EffectInspect.h"

#include "Entity.h"
#include "PromptInformation.h"

EffectInspect::EffectInspect()
{
}


EffectInspect::~EffectInspect()
{
}

void EffectInspect::apply(Entity* target) {
	activePrompt_ = new PromptInformation([this]() { return this->promptExitHandler(); }, target->attr<std::string>("desc"));
}

void EffectInspect::promptExitHandler() {
	delete activePrompt_;
}