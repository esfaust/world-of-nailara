#include "Observable.h"

#include <algorithm>

#include "Input.h"
#include "Locator.h"
#include "Observer.h"


Observable::~Observable() {
	observers_.clear();
}

void Observable::addObserver(Observer* observer) {
	observers_.push_back(observer);
}

void Observable::removeObserver(Observer* observer) {
	observers_.erase(std::remove(observers_.begin(), observers_.end(), observer), observers_.end());
}

void Observable::notify(Event event, InputEvent input) {
	for (unsigned int i = 0; i < observers_.size(); ++i) {
		if (observers_[i])
			observers_[i]->onNotify(event, input);
		else
			removeObserver(observers_[i]);
	}
}

void Observable::notify(Event event, Location loc) {
	for (unsigned int i = 0; i < observers_.size(); ++i) {
		if (observers_[i])
			observers_[i]->onNotify(event, loc);
		else
			removeObserver(observers_[i]);
	}
}

void Observable::notify(Event event) {
	for (unsigned int i = 0; i < observers_.size(); ++i) {
		if (observers_[i])
			observers_[i]->onNotify(event);
		else
			removeObserver(observers_[i]);
	}
}

std::vector<Observer*> Observable::getObservers() {
	return observers_;
}