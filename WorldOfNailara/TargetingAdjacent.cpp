#include "TargetingAdjacent.h"

#include "PromptEntityPicker.h"
#include "Entity.h"
#include "Area.h"
#include "Logger.h"

TargetingAdjacent::TargetingAdjacent(Entity* caller) : Targeting(), caller_(caller)
{
}


TargetingAdjacent::~TargetingAdjacent()
{
}

void TargetingAdjacent::target() {
	std::vector<Entity*> adjEnts = caller_->getLocation().area_->getEntitiesAdjacentTo(caller_->getLocation());
	std::function<void(Entity*)> callback = [this](Entity* e) { return this->promptHandler(e); };
	activePrompt_ = new PromptEntityPicker([this]() { return this->promptExitHandler(); }, callback, adjEnts);
	return;
}

bool TargetingAdjacent::hasValidTargets() {
	std::vector<Entity*> entities = caller_->getLocation().area_->getEntitiesAdjacentTo(caller_->getLocation());

	return !entities.empty();
}

void TargetingAdjacent::applyEffect(Effect* effect) {

	if (!hasValidTargets())
		return;
	try {
		effect->apply(tempTarget_);
	}
	catch (Exception e) {
		typeid(effect).raw_name();
		Locator::getLogger()->log(SEV_INFO, std::string(typeid(this).name()) + " failed to apply effect '" + std::string(typeid(effect).name()));
	}
}

void TargetingAdjacent::promptHandler(Entity* reaction) {
    Locator::getLogger()->log(SEV_INFO, "Prompt Handler");
	tempTarget_ = reaction;
	applyEffect(tempEffect_);
    delete activePrompt_;
}

void TargetingAdjacent::targetAndApply(Effect* effect) {
	tempEffect_ = effect;
	std::vector<Entity*> adjEnts = caller_->getLocation().area_->getEntitiesAdjacentTo(caller_->getLocation());
	std::function<void(Entity*)> callback = [this](Entity* e) { return this->promptHandler(e); };
	activePrompt_ = new PromptEntityPicker([this]() { return this->promptExitHandler(); }, callback, adjEnts);
	return;
}

void TargetingAdjacent::promptExitHandler() {
    delete activePrompt_;
}