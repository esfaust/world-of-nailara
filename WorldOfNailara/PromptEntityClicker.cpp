#include "PromptEntityClicker.h"

#include "Display.h"
#include "Locator.h"
#include "Exception.h"

#include "PromptLocationClicker.h"
#include "PromptEntityPicker.h"

PromptEntityClicker::PromptEntityClicker(std::function<void()> exitCallback, std::function<void(Entity*)> callback) : PromptWindow(exitCallback), callback_(callback)
{
	std::function<void(Location)> loccallback = [this](Location e) { return this->promptHandler(e); };
	activePrompt_ = new PromptLocationClicker([this]() { return this->promptExitHandler(); }, loccallback);
}

void PromptEntityClicker::draw() {
}

void PromptEntityClicker::promptHandler(Location clickLoc) {
	std::vector<Entity*> entities = clickLoc.area_->getEntitiesAt(clickLoc);
	if (entities.size() <= 0) {
		//Don't destroy the prompt, so it can try again
		Locator::getLogger()->log(SEV_INFO, "No entities found.");
	} else if (entities.size() == 1) {
		//Successfully clicked location, delete prompt
		callback_(entities[0]);
		
		//delete activePrompt_;
	} else if (entities.size() > 1) {
		//Successfully clicked location, delete prompt
		delete activePrompt_;
		Locator::getLogger()->log(SEV_INFO, "Many entities found.");
		std::function<void(Entity*)> entcallback = [this](Entity* e) { return this->promptHandler(e); };
		activePrompt_ = new PromptEntityPicker([this]() { return this->promptExitHandler(); }, entcallback, entities);
	}
}

void PromptEntityClicker::promptExitHandler() {
	delete activePrompt_;
	exitCallback_();
}

void PromptEntityClicker::promptHandler(Entity* ent) {
	callback_(ent);
	delete activePrompt_;
}