#pragma once

#include "Area.h"

// Small testing area with AI, Objects, Walls and such
class AreaTesting :
	public Area
{
public:
	AreaTesting();
	~AreaTesting();
};

