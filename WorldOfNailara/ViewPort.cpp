#include "ViewPort.h"

#include "Display.h"
#include "Locator.h"
#include "Exception.h"

#include "Tile.h"

ViewPort::ViewPort(int x, int y, int width, int height) : width_(width), height_(height), offsetX_(x), offsetY_(y) {
	focalPoint_ = new FocalPoint(2,2);

    fitTilesHorizontally = width_ / TILEWIDTH;
    fitTilesVertically = height_ / TILEHEIGHT;
    evenX_ = (fitTilesHorizontally % 2) == 0;
    evenY_ = (fitTilesVertically % 2) == 0;
    paddingX_ = width_ / 2 / TILEWIDTH;
    paddingY_ = height_ / 2 / TILEHEIGHT;
}

void ViewPort::setActiveArea(Area *a) {
	activeArea_ = a;
};

// Sets the focal point of the map
void ViewPort::focalTracking(Entity* ent) {
    focalPoint_->setTracking(ent);
};

bool ViewPort::isInRenderArea(int x, int y) {
	// If even view size, chop off right or bottom most
    x = x * TILEWIDTH;
    y = y * TILEHEIGHT;

    return true; 
    if (x + TILEWIDTH > width_) {
        return false;
    }
    if (y + TILEHEIGHT > height_) {
        return false;
    }

	return true;
}

int ViewPort::translateX(int x) {
	// Calculate offset, so it can render around the focal point
	int ofx = x - focalPoint_->x_;
	//Calculate where to render
    return 1;
}

int ViewPort::translateY(int y) {
	// Calculate offset, so it can render around the focal point
	int ofy = y - focalPoint_->y_;

	//Calculate where to render
	return offsetY_ + ((ofy + paddingY_) * (TILEWIDTH / ASCIIFONTHEIGHT));
}

Location ViewPort::translateViewPortCoordinate(int x, int y) {
	int dx, dy;
	dx = x - paddingX_ + focalPoint_->x_;
	dy = y - paddingY_ + focalPoint_->y_;
	return Location(activeArea_, dx, dy);
}

void ViewPort::draw() {
    // Determine what tiles are around the view point
    int startX, endX, startY, endY;
    int adjustmentX = std::abs(std::min(focalPoint_->x_ - paddingX_, 0));
    int adjustmentY = std::abs(std::min(focalPoint_->y_ - paddingY_, 0));

    startX = std::max(focalPoint_->x_ - paddingX_, 0);
    endX = std::min(focalPoint_->x_ + paddingX_ - evenX_, (int)activeArea_->getWidth() - 1);

    startY = std::max(focalPoint_->y_ - paddingY_, 0);
    endY = std::min(focalPoint_->y_ + paddingY_ - evenY_, (int)activeArea_->getHeight() - 1);

    std::vector<std::vector<Tile>> tiles = activeArea_->getTiles(startX, startY, endX, endY);
    
    int fw = fitTilesHorizontally * TILEWIDTH;
    int fh = fitTilesVertically * TILEHEIGHT;

    for (int x = 0; x <= endX - startX; ++x) {
        for (int y = 0; y <= endY - startY; ++y) {
            tiles[x][y].render(offsetX_ + (x + adjustmentX) * TILEWIDTH, offsetY_ + (y + adjustmentY) * TILEHEIGHT);
        }
    }
}

bool ViewPort::mouseInViewPort(int mx, int my) {
	if (mx >= offsetX_ && mx < offsetX_ + width_ ) {
		if (my >= offsetY_ && my < offsetY_ + height_) {
			return true;
		}
	}
	return false;
}

std::unordered_map<std::string, int> ViewPort::getInfo() {
	std::unordered_map<std::string, int> info;
	info["offsetX"] = offsetX_;
	info["offsetY"] = offsetY_;
	info["width"] = width_;
	info["height"] = height_;
	return info;
}

Area* ViewPort::getArea() {
	return activeArea_;
}