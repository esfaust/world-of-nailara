#include "TextureArray.h"

#include <SOIL.h>

TextureArray::TextureArray(std::string file)
{
    load(file);
}


TextureArray::~TextureArray()
{
}


void TextureArray::load(std::string file) {
    glGenTextures(1, &ID_);
    glBindTexture(GL_TEXTURE_2D, ID_); // All upcoming GL_TEXTURE_2D operations now have effect on our texture object

                                       // Load, create texture and generate mipmaps
    unsigned char* image = SOIL_load_image(file.c_str(), &width_, &height_, 0, SOIL_LOAD_RGBA);
    //glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width_, height_, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
    //glGenerateMipmap(GL_TEXTURE_2D);
    glTexStorage3D(GL_TEXTURE_2D_ARRAY, 1, GL_RGBA, width_, height_, 1);
    glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, 0, width_, height_, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
    SOIL_free_image_data(image);

    //create a TEXTURE_2D_ARRAY, making sure the texture width and height are the dimensions of your largest texture.
    //The last argument is NULL as we want to iterate through our images and put them in the correct position in our texture array using glTexSubImage
    

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// Set texture wrapping to GL_REPEAT
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    // Set texture filtering
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glBindTexture(GL_TEXTURE_2D, 0);
}

void TextureArray::bind(int activeTexture) {
    switch (activeTexture) {
    case 0:
        glActiveTexture(GL_TEXTURE0);
        break;
    case 1:
        glActiveTexture(GL_TEXTURE1);
        break;
    default:
        glActiveTexture(GL_TEXTURE0);
        break;
    }

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, ID_);
}