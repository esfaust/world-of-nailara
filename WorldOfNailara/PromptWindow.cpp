#include "PromptWindow.h"

#include "Locator.h"
#include "Event.h"
#include "Display.h"
#include "Input.h"
#include "Renderer.h"
#include "PromptWindow.h"

PromptWindow::PromptWindow(std::function<void()> exitCallback) : exitCallback_(exitCallback)
{
	Locator::getInput()->addObserver(this);
	Locator::getDisplay()->getPrompts()->addPrompt(this);
	Locator::getInput()->getPrompts()->addPrompt(this);
}


PromptWindow::~PromptWindow()
{
	Locator::getInput()->removeObserver(this);
	Locator::getInput()->getPrompts()->removePrompt(this);
	Locator::getDisplay()->getPrompts()->removePrompt(this);
}

void PromptWindow::draw() {
}

void PromptWindow::promptExitHandler() {
	delete activePrompt_;
}