#include "SpriteSheet.h"

#include <vector>

#include "Renderer.h"
#include "Texture.h"
#include "Shader.h"
#include "Sprite.h"
#include "Locator.h"
#include "Display.h"

SpriteSheet::SpriteSheet(Texture* texture, Shader* shader, int spriteWidth, int spriteHeight) : texture_(texture), shader_(shader), spriteWidth_(spriteWidth), spriteHeight_(spriteHeight) {
    sheetWidth_ = texture_->getWidth();
    sheetHeight_ = texture_->getHeight();

	tw_ = float(spriteWidth_) / sheetWidth_;
	th_ = float(spriteHeight_) / sheetHeight_;

    numPerRow_ = int(sheetWidth_ / spriteWidth_);
}


SpriteSheet::SpriteSheet(std::string textureFile, std::string vertexShaderFile, std::string fragmentShaderFile, int spriteWidth, int spriteHeight) {
	SpriteSheet(new Texture(textureFile), new Shader(vertexShaderFile, fragmentShaderFile), spriteWidth, spriteHeight);
}


void SpriteSheet::draw(int x, int y, int z, int index) {
    if (renderer_ == NULL) {
        renderer_ = Locator::getDisplay()->getRenderer();
    }

	float tx = (index % numPerRow_) * tw_;
	float ty = std::floor(index / numPerRow_) * th_;
	float tc[8] = {
 	    1.0f - (tx + tw_), 1.0f - (ty + th_)    //A
	    , 1.0f - (tx + tw_), 1.0f - (ty)        //B
	    , 1.0f - (tx), 1.0f - (ty)              //C
	    , 1.0f - (tx), 1.0f - (ty + th_)        //D
	};

	renderer_->drawQuad(x, y, z, spriteWidth_, spriteHeight_, shader_, texture_, tc);
}

void SpriteSheet::draw(int x, int y, int z, int index, int width, int height) {
    if (renderer_ == NULL) {
        renderer_ = Locator::getDisplay()->getRenderer();
    }

    float tx = (index % numPerRow_) * tw_;
    float ty = std::floor(index / numPerRow_) * th_;
    float tc[8] = {
        1.0f - (tx + tw_), 1.0f - (ty + th_)    //A
        , 1.0f - (tx + tw_), 1.0f - (ty)        //B
        , 1.0f - (tx), 1.0f - (ty)              //C
        , 1.0f - (tx), 1.0f - (ty + th_)        //D
    };
    renderer_->drawQuad(x, y, z, width, height, shader_, texture_, tc);
}

void SpriteSheet::queue(int spriteIndex, int posX, int posY, int z) {
    if (renderer_ == NULL) {
        renderer_ = Locator::getDisplay()->getRenderer();
    }
    renderer_->queueSpriteSheet(posX, posY, z, spriteWidth_, spriteHeight_, this, spriteIndex);
}

void SpriteSheet::queue(int spriteIndex, int posX, int posY, int z, int width, int height) {
    if (renderer_ == NULL) {
        renderer_ = Locator::getDisplay()->getRenderer();
    }
    renderer_->queueSpriteSheet(posX, posY, z, width, height, this, spriteIndex);
}

Sprite* SpriteSheet::generateSprite(int spriteIndex) {
    const int numPerRow = int(sheetWidth_ / spriteWidth_);
    const int numRows = int(sheetHeight_ / spriteHeight_);
    if (spriteIndex < numPerRow * numRows) {
        return new Sprite(this, spriteIndex);
    }
    else {
        return new Sprite(this, 0);
    }
}

int SpriteSheet::getSpriteHeight() {
    return spriteHeight_;
}

int SpriteSheet::getSpriteWidth() {
    return spriteWidth_;
}

Texture* SpriteSheet::getTexture() {
    return texture_;
}

Shader* SpriteSheet::getShader() {
    return shader_;
}