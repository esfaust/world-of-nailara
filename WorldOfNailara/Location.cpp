#include "Location.h"

#include <math.h>

#include "Area.h"
#include "Location.h"

bool operator == (Location const& lhs, Location const& rhs)
{
    if (!lhs.area_) return false;
	return (lhs.x_ == rhs.x_) &
		(lhs.y_ == rhs.y_) &
		(lhs.area_ == rhs.area_);
}

bool operator != (Location const& lhs, Location const& rhs)
{
    if (!lhs.area_) return true;
	return !((lhs.x_ == rhs.x_) &
		(lhs.y_ == rhs.y_) &
		(lhs.area_ == rhs.area_));
}

float Location::distance(Location t) {
    if (!area_) return INT_MAX;

	if (t.area_ != area_)
		return 9999;
	int dx = t.x_ - x_;
	int dy = t.y_ - y_;
	return (float)sqrt(dx*dx + dy*dy);
}

void Location::shift(int dx, int dy) {
    if (!area_) return;
    x_ += dx;
    y_ += dy;
}