﻿#include "Display.h"

#include "Locator.h"
#include "UiFantasy.h"
#include "RendererOpenGL.h"
#include "PromptTracker.h"
#include "BorderedViewPort.h"

Display::Display(std::string name, int windowWidth, int windowHeight, int viewportWidth, int viewportHeight)
{
	Locator::getLogger()->log(SEV_INFO, "Display initialized.");

    viewPort_ = new BorderedViewPort(0, 0, 288, 288);
	renderer_ = new RendererOpenGL("World of Nailara", windowWidth, windowHeight);
	ui_ = new UiFantasy(renderer_, viewPort_);
	prompts_ = new PromptTracker();
	asciiMode_ = false;
	height_ = windowHeight;
	width_ = windowWidth;
}

Display::~Display() {
    delete viewPort_;
	delete renderer_;
}

void Display::render() {
	renderer_->clear();
	viewPort_->draw();
	ui_->draw();
	prompts_->draw();
	renderer_->render();
}

bool Display::isInView(int mapX, int mapY) {
	return viewPort_->isInRenderArea(mapX, mapY);
}

void Display::setLayer(int layer) {
	renderer_->setLayer(layer);
}

void Display::setFocalPoint(FocalPoint fp) {
	//viewPort_.setFocalPoint(fp);
}

void Display::setActiveArea(Area* a) {
	viewPort_->setActiveArea(a);
}

Area* Display::getActiveArea() {
	return viewPort_->getArea();
}


std::unordered_map<std::string, int> Display::getViewportInfo() {
	return viewPort_->getInfo();
}

bool Display::mouseInViewPort(int mx, int my) {
	return viewPort_->mouseInViewPort(mx, my);
}

Location Display::translateViewPortCoordinate(int x, int y) {
	return viewPort_->translateViewPortCoordinate(x, y);
}