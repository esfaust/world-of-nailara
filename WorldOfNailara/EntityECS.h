#pragma once

#include <vector>

#include "Attributes.h"
#include "Locator.h"
#include "Location.h"

class Mind;
class Memory;
class Perception;
class Body;
class Sprite;

/*
An entity with a mind can act or react to other things of its own accord, if its form allows.
A mind can only react to what it perceives (Perception component) or remembers (Memory component)
*/
class Mind {
private:
    Memory* memory_;
};

class Sight;
class Hearing;
class Smell;
class Perception {
public:
    void refresh(); // Checks for any new ways of perceiving the world
    void percieve(); // Attempt to use all of the perception types to notice things about the world
private:
    Sight* sight_; // Sight, darkvision, light blindness, etc
    Hearing* hearing_;
    Smell* smell_;
};

class EntityECS : public Attributes
{
public:
    EntityECS(std::string name, Sprite* ap, Location l);
    ~EntityECS();

    void render(); // Render the entity if its body is renderable

private:
    std::string name = "undefined"; // Name of the entity
    uint32_t id_; // The unique ID of the entity
    
    Body *body_; // Location Form, Collisions, physical attributes, current health, etc
    Perception *perception_; // Sight, Hearing, Smell, etc. Draws from both body (usin' your eyeballs, nose, ears) and mind (any magical abilities that let you detect).
    Mind *mind_; // Personality, desires, memory, etc
};


