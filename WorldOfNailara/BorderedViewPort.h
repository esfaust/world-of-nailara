#pragma once

#include "ViewPort.h"

// A view into an area that draws a nice border around its edges
class BorderedViewPort : 
    public ViewPort
{
public:
    BorderedViewPort(int x, int y, int width, int height);
    void draw();

protected:
    int borderWidth_;
    int interiorWidth_;
    int interiorHeight_;
};

