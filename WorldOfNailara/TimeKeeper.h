#pragma once

#include "Actor.h"
#include <vector>

class TimeKeeper
{
public:
	TimeKeeper() {};
	~TimeKeeper() {};

	void registerActor(Actor* a);
	void releaseActor(Actor* a);

	void tick();

private:
	std::vector<Actor*> travelers;
};
