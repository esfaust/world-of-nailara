#pragma once

#include <vector>
#include <map>

#include "Color.h"

#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/GLU.h>

// GLFW
#include <GLFW/glfw3.h>
#include <SOIL.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Font.h"

class StringRenderer
{
public:
    StringRenderer(GLFWwindow* window, glm::mat4 projection);

	void drawText(int x, int y, int z, std::string text, Font* font, float scale, Color color = Color());
	void drawChar(int x, int y, int z, char ch, Font* font, float scale, Color color = Color());

    Character getCharacter(char c);

private:
    void drawCharacter(Character ch, int x, int y, int z, float scale, Color color = Color());

    GLuint textVAO_, textVBO_, textEBO_;
    GLFWwindow* window_;

    std::map<char, Character> characters_;

    glm::mat4 projection_;
};

