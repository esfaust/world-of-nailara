#pragma once
#include "Targeting.h"

#include "PromptEntityClicker.h"

class TargetingClickEntity :
	public Targeting
{
public:
	TargetingClickEntity();
	~TargetingClickEntity();

	void target();
	bool hasValidTargets();
	void applyEffect(Effect* effect);
	void targetAndApply(Effect* effect);

private:
	void promptHandler(Entity* chosenEntity);
	void promptExitHandler();

	Entity* tempTarget_;
	Effect* tempEffect_;
};

