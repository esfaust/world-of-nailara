#pragma once

#include <vector>

#include "Tile.h"
#include "FocalPoint.h"
#include "TimeKeeper.h"

class Entity;
class Actor;

/*
An area is a physical region that entities can dwell in, and can be rendered
All areas are finite in size, and are made up of tiles of a certain size.
*/
class Area
{
public:
	// Constructor to define the size of the Area, and the tile size
	Area(int w, int h);
	~Area();

	// Tell all of the tiles in the area to render themselves, and by extension everything on the tiles
	void render();
	// Tells all of the entities in the area to update
	void update();

	// Return the height
	unsigned int getHeight() const { return height_; };
	// Return the width
	unsigned int getWidth() const { return width_; };

	// Begin or stop tracking an active entity
	void addActor(Actor* entity);
	void removeActor(Actor* entity);

	// Begin tracking a passive entity
	void addEntity(Entity* entity, bool passive = true);
	void removeEntity(Entity* entity);

	// Shift an entity's location by dX, dY. Must be on this map
	Location shiftEntity(Entity* entity, int dx, int dy);
	// Teleport an entity on this map to another location, potentially out of the map
	Location teleportEntity(Entity* entity, Location newLocation);
	Location teleportActor(Actor* entity, Location newLocation);

	// Returns whether this is a valid location for an entity to be (is within bounds)
	bool isValidLocation(Location target);
	Blocking::Set getBlockingAt(Location target);

	// Returns a list of all entities at a target location
	std::vector<Entity*> getEntitiesAt(Location target);
	std::vector<Entity*> getEntitiesAdjacentTo(Location target);

	void setFocusTracking(Observable* target);
	FocalPoint getFocalPoint() { return focalPoint_; }


    std::vector<std::vector<Tile>> getTiles(int startX, int startY, int endX, int endY);

protected:
	// Checks whether a target location is within bounds
	bool withinBounds(Location target);

	// Height of the area in tiles
	unsigned int height_, width_;
	// Pixel size of each tile (square tiles)
	unsigned int tileSize_ = 64;

	// Pointer towards the location that the camera should track
	FocalPoint focalPoint_;

	// Tiles that make up the area to display
	//std::vector<Tile> tiles_;
	std::vector<std::vector<Tile>> tiles_;
	// Entities that require updating actively. Creatures, the player
	std::vector<Actor*> actors_;
	// Entities that exist, but do not act on their own. Objects, doors, furniture, exits
	std::vector<Entity*> activeEntities_;
	std::vector<Entity*> passiveEntities_;

	TimeKeeper tk_;
};

