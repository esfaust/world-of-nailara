#include "Drawable.h"

#include "Locator.h"
#include "Display.h"
#include "Renderer.h"
#include "RenderItem.h"
#include "Library.h"
#include "Sprite.h"

void Drawable::dequeue() {
	Locator::getDisplay()->getRenderer()->destroy(queueData_);
}

Drawable::~Drawable() {
	dequeue();
	delete queueData_;
}


ExampleDrawable::ExampleDrawable(Sprite* sprite, Shader* shader) {
	sprite_ = sprite;
	shader_ = shader;
}

void ExampleDrawable::queue(int x, int y, int z) {
	queueData_ = new RenderItemSprite(x,y,z,shader_, sprite_);
	Locator::getDisplay()->getRenderer()->add(queueData_);
}





DrawableBody::DrawableBody(Body* parent, Sprite* sprite) : parent_(parent), sprite_(sprite) {

}

void DrawableBody::queue(int x, int y, int z) {
	queueData_ = new RenderItemSprite(x, y, z, shader_, sprite_);
	Locator::getDisplay()->getRenderer()->add(queueData_);
}