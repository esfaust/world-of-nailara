#pragma once

#include <vector>

#include "Entity.h"
#include "Effect.h"

class PromptWindow;
/*
This class handles all of the targeting for abilities.
'Target' is called to either request the player's input on who to target, or
simply targets them on its own
*/
class Targeting
{
public:
	Targeting() {}
	virtual void target()=0;
	virtual bool hasValidTargets()=0;
	virtual void applyEffect(Effect* effect) = 0;
	virtual void targetAndApply(Effect* effect) = 0;

protected:
    virtual void promptExitHandler();

	PromptWindow* activePrompt_;
};

