#pragma once

#include <string>

/*
Common static class to provide useful helper functions
*/
class Common
{
public:
	// Returns a string that describes the current time
	static std::string now_str();

	// Returns the current time in milliseconds
	static unsigned long time_ms();

	static double time();
};

