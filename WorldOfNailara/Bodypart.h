#pragma once

#include "Attributes.h"
#include "BodypartConnection.h"

#include <string>
#include <unordered_map>

class Bodypart : public Attributes {
public:
    Bodypart(std::string name, BodypartConnector::Type connector) : name_(name), connectorType_(connector), connectors_({}) {};
    Bodypart(std::string name, BodypartConnector::Type connector, std::unordered_map<BodypartConnector::Type, int> connectors) : name_(name), connectorType_(connector), connectors_(connectors) {};
    Bodypart(std::string name, std::string description, BodypartConnector::Type connector) : name_(name), connectorType_(connector), connectors_({}), description_(description) {};
    Bodypart(std::string name, std::string description, BodypartConnector::Type connector, std::unordered_map<BodypartConnector::Type, int> connectors) : name_(name), connectorType_(connector), connectors_(connectors), description_(description) {};
    BodypartConnector::Type connectorType();
    int connectorCount(BodypartConnector::Type type);
    std::string getDescriptionTemplate();
private:
    std::string name_;
    std::string description_; // A description, if any, when examining this creature
    BodypartConnector::Type connectorType_; // What connection this bodypart can connect to, ex) Head->Neck
    
    std::unordered_map<BodypartConnector::Type, int> connectors_; // Connectors and how many of them exist
};