#pragma once
#include "Effect.h"

#include <string>

class PromptWindow;

//TODO: Check whether they have talk ability
//TODO: Determine dialogue more intelligently

// Activates the speech effect, talking to an entity if they are a valid conversational partner
class EffectSpeech :
	public Effect
{
public:
	EffectSpeech(std::string dialogue="Hello, world!");
	~EffectSpeech();
	void apply(Entity* entity);

private:
	void promptHandler(std::string response);
	void promptExitHandler();

	std::string dialogue_;
	PromptWindow* activePrompt_;
	bool waitingOnPrompt_ = false;
};

