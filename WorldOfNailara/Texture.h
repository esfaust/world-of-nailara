#pragma once

#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/GLU.h>

#include <string>

class Texture
{
public:
	Texture(std::string file);
	Texture();
	~Texture();

	void load(std::string file);
	void bind(int activeTexture=0);

    GLuint ID_;

    int getWidth() { return width_; }
    int getHeight() { return height_; }

private:
    int width_, height_;
};

