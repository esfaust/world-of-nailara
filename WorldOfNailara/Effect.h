#pragma once

#include "Exception.h"
#include "Location.h"

class Entity;

/*
This interface class is used to apply effects to targets, and is used by abilities.
Because effects might be on an entity, set of locations, or even another effect,
the Targeting class holds whatever variables might be passed in.
*/
class Effect
{
public:
	// Applies the effect to a target, or throws an exception if its the wrong kind of target for the effect
	virtual void apply(Entity* entity) {
		throw Exception("Effect application on 'Entity*' not defined.\n");
	}
	virtual void apply(Location location) {
		throw Exception("Effect application on 'Location' not defined.\n");
	}
	//'ticks' the effect, for ongoing effects like damage over time or duration
	virtual void tick() {};
};