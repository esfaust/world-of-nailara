#pragma once

#include <string>
#include <vector>
#include <memory>

#include "BlockingFlags.h"
#include "GraphicalLayering.h"

class Sprite;
class Entity;

// This class tracks everything about a certain tile in the world
class Tile
{
public:
	Tile() {}
	Tile(Sprite* app, int ix, int iy) : appearance_(app), x_(ix), y_(iy), explored_(false) {}
	Tile(Sprite* app, int ix, int iy, Blocking::Set blocks) : appearance_(app), x_(ix), y_(iy), explored_(false), blocks_(blocks) {}
	~Tile();

	// Has the player seen this tile before?
	bool isExplored() { return explored_; }
	// Set whether this tile has been explored or not
	void setExplored(bool b) { explored_ = b; }
	// Render the tile in its correct place on the map
	void render(int x, int y, int z = GraphicalLayering::LAYER_TERRAIN);
	// Start/Stop tracking an entity
	void addEntity(Entity* e);
	void removeEntity(Entity* e);

	// Retrieve a full list of all entities on the tile
	std::vector<Entity*> getEntities();

	Blocking::Set getBlocking() { return blocks_; }
private:
	// The location of the tile, passed in at the very beginning
	// A tile should never move
	int x_, y_;
	// Has the player seen this tile before?
	bool explored_;
	// The appearance to render
    Sprite* appearance_;
	//Entities located on this tile
	std::vector<Entity*> entities_;
	
	Blocking::Set blocks_;
};

