#pragma once

#include <memory>
#include <vector>

#include "Event.h"
#include "InputEvent.h"
#include "Location.h"

class Observer;

#define MAX_OBSERVERS 100

// This class denotes that a class is observable - things can subscribe to get events from it
// When something of note happens, it will inform all of its observers
class Observable
{
public:
	~Observable();

	// Adds an observer to alert if something happens with this subject
	void addObserver(Observer *observer);
	// Stops alerting a certain observer of events with this subject
	void removeObserver(Observer *observer);

	// Notifies an observer that an event occured with this set of data
	// If this is a set of data that sounds interesting, the class can define their onNotify for it
	// In the Event class is the specific event type
	void notify(Event event, InputEvent input);
	void notify(Event event, Location location);
	void notify(Event event);

	std::vector<Observer*> getObservers();

protected:
	// The list of observers that have registered interest
	std::vector<Observer*> observers_;
};

