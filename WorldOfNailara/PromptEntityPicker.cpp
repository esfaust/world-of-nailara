﻿#include "PromptEntityPicker.h"

#include "Locator.h"
#include "Display.h"
#include "Input.h"
#include "Locator.h"
#include "Display.h"
#include "Renderer.h"
#include "Color.h"
#include "RenderQueueItem.h"
#include "GraphicalLayering.h"

PromptEntityPicker::PromptEntityPicker(std::function<void()> exitCallback, std::function<void(Entity*)> callback, std::vector<Entity*> choices) : PromptWindow(exitCallback), callback_(callback), choices_(choices)
{
	if (choices.empty()) {
		delete this;
		return;
	}

    Display* disp = Locator::getDisplay();
    int w = Locator::getDisplay()->getRenderer()->getWidth();
    int h = Locator::getDisplay()->getRenderer()->getHeight();

    x_ = int(w / 15);
    width_ = int(x_ * 13);
    y_ = int(h / 15);
    height_ = int(y_ * 13);
}

void PromptEntityPicker::draw() {
    Locator::getDisplay()->getRenderer()->queueQuadBordered(x_, y_, GraphicalLayering::LAYER_PROMPT, width_, height_, Locator::getLibrary<Shader*>()->get<Shader*>("basic"), Locator::getLibrary<Texture*>()->get<Texture*>("bg2"), Locator::getLibrary<Texture*>()->get<Texture*>("borderh"));

	int adjust = 0;
	char label = 'a';
	for (auto choice : choices_) {
        Color col = Color(1.0f, 0.0f, 0.0f);
		if (highlighted_ == adjust) {
            col = Color(0.0f, 1.0f, 0.0f);
		}
		//choice->attr<int>("name");
		std::string choiceName;
		choiceName.push_back(label);
		choiceName += ") " + choice->attr<std::string>("name");

        Locator::getDisplay()->getRenderer()->queueText(x_ + 16, y_ + 16 + (adjust * 32), GraphicalLayering::LAYER_PROMPT_TEXT, choiceName, Locator::getLibrary<Font*>()->get<Font*>("arial-bold"), 0.5f, col, Locator::getLibrary<Shader*>()->get<Shader*>("text"));
        adjust++;
		label++;
	}
}

void PromptEntityPicker::onNotify(Event event, InputEvent input) {
	if (input.key == IK_ESCAPE) {
        if (input.action == IK_PRESS)
		    exitCallback_();
	} else if (input.key == IK_DOWN) {
		if (highlighted_ != choices_.size()-1) {
            if (input.action == IK_PRESS)
			    ++highlighted_;
		}
	} else if (input.key == IK_UP) {
		if (highlighted_ != 0) {
            if (input.action == IK_PRESS)
			    --highlighted_;
		}
	}
	else if (input.key == IK_ENTER) {
        if (input.action == IK_PRESS) {
            Locator::getLogger()->log(SEV_INFO, "EntityPicker Enter");
            callback_(choices_[highlighted_]);
        }
	}
}