#pragma once
#include <unordered_map>
#include <string>
#include <boost\variant.hpp>

#include <string>
#include <cstdlib>

#include <boost/type_index.hpp> // Boost

class IComponent
{
public:

    template<typename T> void attr(std::string key, T value) {
        attributes_[key] = value;
    };

    template<typename T> T attr(std::string key) {
        try {
            if (attributes_.count(key) != 0) {
                return boost::get<T>(attributes_[key]);
            }
            else {
                return T();
            }
        }
        catch (boost::bad_get&) {
            Locator::getLogger()->log(SEV_FATAL, "Invalid type '"+ boost::typeindex::type_id<T>().pretty_name() + "' of attribute '" + key + "'");
            return T();
        }
    }

private:
    std::unordered_map<std::string, boost::variant<std::string, int>> attributes_;
};

