#include "EffectSpeech.h"

#include "PromptInformation.h"

EffectSpeech::EffectSpeech(std::string dialogue)
{
	dialogue_ = dialogue;
}


EffectSpeech::~EffectSpeech()
{
}

#include "PromptDialogue.h"
#include "Portrait.h"
#include "Locator.h"
void EffectSpeech::apply(Entity* entity) {
    Portrait *portrait = new Portrait(Locator::getLibrary<Sprite*>()->get<Sprite*>("kona"));
    activePrompt_ = new PromptDialogue([this]() { return this->promptExitHandler(); }, portrait, "Hi! My name is Kona!");
}

void EffectSpeech::promptHandler(std::string s) {
	delete activePrompt_;
	
}

void EffectSpeech::promptExitHandler() {
	delete activePrompt_;
}