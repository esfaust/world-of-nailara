#version 330 core
layout (location = 0) in vec3 position;

out vec4 fColor;
out vec2 fCoord;

uniform mat4 model;
uniform mat4 projection;
uniform mat4 tcord;
uniform vec4 color;
uniform vec2 textureCoordinates[4];
uniform float layer;

void main()
{
	gl_Position = projection * model * vec4(position, 1.0f);
	fColor = color;
	
	// Don't forget to flip everything so it it rightside up
	fCoord = vec2(1 - textureCoordinates[gl_VertexID].x, 1 - textureCoordinates[gl_VertexID].y);
}