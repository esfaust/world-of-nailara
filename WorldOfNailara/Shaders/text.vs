#version 330 core
layout (location = 0) in vec3 position;
layout(lines_adjacency) in;

out vec4 fColor;
out vec2 fCoord;

uniform mat4 model;
uniform mat4 projection;
uniform vec4 color;
uniform vec2 textureCoordinates[4];
uniform float time;
uniform float centerX;

void main()
{
	gl_Position = projection * model * vec4(position, 1.0f);
	
	//gl_Position = gl_Position + vec4(0.0f, max(sin((time*8 + (centerX)))/16.0f, 0), 0.0f, 0.0f);
	
	//vec4 gnarly = vec4(100.0f, 100.0f, 100.0f, 0.5f);
	
	fColor = color;
	
	// Don't forget to flip everything so it it rightside up
	fCoord = vec2(1 - textureCoordinates[gl_VertexID].x, 1 - textureCoordinates[gl_VertexID].y);
}