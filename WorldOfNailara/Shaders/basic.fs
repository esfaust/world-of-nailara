#version 330 core
in vec4 fColor;
in vec2 fCoord;

out vec4 color;

// Texture samplers
uniform sampler2D fragTexture;

void main()
{
	// Linearly interpolate between both textures (second texture is only slightly combined)
	color = texture(fragTexture, fCoord) * fColor;
}