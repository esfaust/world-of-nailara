#include "Locator.h"

#include <vector>
#include <unordered_map>

#include "Display.h"
#include "Input.h"
#include "Logger.h"
#include "Observer.h"

void Locator::provide(Input *service) { input_ = service; }
void Locator::provide(Display *service) { display_ = service; }
void Locator::provide(Logger *service) { logger_ = service; }

Display* Locator::getDisplay() {
    if (display_ == NULL) {
        static NullDisplay nd;
        return &nd;
    }
    else
        return display_;
}

Input* Locator::getInput() {
    if (input_ == NULL) {
        static NullInput ni;
        return &ni;
    }
    else
        return input_;
}

Logger* Locator::getLogger() {
    if (logger_ == NULL) {
        static NullLogger nl;
        return &nl;
    }
    else
        return logger_;
}

Display* Locator::display_ = NULL;
Input* Locator::input_ = NULL;
Logger* Locator::logger_ = NULL;

std::unordered_map<std::type_index, LibraryGeneric*> Locator::libs_ = {};