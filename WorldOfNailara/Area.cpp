#include "Area.h"

#include "Actor.h"
#include "Display.h"
#include "Entity.h"
#include "Exception.h"
#include "FocalPoint.h"
#include "Locator.h"

#include <iostream>
#include <algorithm>


Area::Area(int w, int h) : width_(w), height_(h)
{

	std::vector<std::vector<Tile>> vec(width_, std::vector<Tile>(height_));
	tiles_ = vec;
	for (unsigned int i = 0; i < width_; i++) {
		for (unsigned int j = 0; j < height_; j++) {
			Tile t = Tile(Locator::getLibrary<Sprite*>()->get<Sprite*>("floor"), i, j);
			tiles_[i][j] = t;
		}
	}
}


Area::~Area()
{
}


void Area::render() {
	Locator::getDisplay()->setFocalPoint(focalPoint_);
	for (unsigned int x = 0; x < width_; ++x) {
		for (unsigned int y = 0; y < height_; ++y) {
			tiles_[x][y].render(32, 32);
		}
	}
}

void Area::update() {
	tk_.tick();

	/*for (auto entity : entities_) {
		entity->update();
	}*/
}

void Area::addActor(Actor* entity) {
	actors_.push_back(entity);
	tk_.registerActor(entity);

	Location l = entity->getLocation();

	if ( 0 <= l.x_ && l.x_ < width_ && 0 <= l.y_ && l.y_ < height_)
		tiles_[l.x_][l.y_].addEntity(entity);
}

void Area::addEntity(Entity* entity, bool passive) {
	if (passive) {
		passiveEntities_.push_back(entity);
	}
	else {
		activeEntities_.push_back(entity);
	}

	Location l = entity->getLocation();

	if (0 <= l.x_ && l.x_ < width_ && 0 <= l.y_ && l.y_ < height_)
		tiles_[l.x_][l.y_].addEntity(entity);
}

void Area::removeActor(Actor* entity) {
	Location oldLoc = entity->getLocation();
	tiles_[oldLoc.x_][oldLoc.y_].removeEntity(entity);

	actors_.erase(std::remove(actors_.begin(), actors_.end(), entity), actors_.end());
}

void Area::removeEntity(Entity* entity) {
	Location oldLoc = entity->getLocation();
	tiles_[oldLoc.x_][oldLoc.y_].removeEntity(entity);
	passiveEntities_.erase(std::remove(passiveEntities_.begin(), passiveEntities_.end(), entity), passiveEntities_.end());
}


Location Area::shiftEntity(Entity* entity, int dx, int dy) {
	Location oldLoc = entity->getLocation();
	Location newLoc = Location(oldLoc.area_, oldLoc.x_ + dx, oldLoc.y_ + dy);
	if (isValidLocation(newLoc)) {
		tiles_[newLoc.x_][newLoc.y_].addEntity(entity);
		tiles_[oldLoc.x_][oldLoc.y_].removeEntity(entity);
		return newLoc;
	}
	return oldLoc;
}


bool Area::isValidLocation(Location target) {
	//Potentially other calculations, like whether the target tile has a floor there
	return withinBounds(target);
}


Blocking::Set Area::getBlockingAt(Location target) {
	if (withinBounds(target)) {
		return tiles_[target.x_][target.y_].getBlocking();
	}
	else {
		return Blocking::S_None;
	}
}


bool Area::withinBounds(Location target) {
	return (target.x_ < width_ && target.y_ < height_);
}


Location Area::teleportEntity(Entity* entity, Location newLoc) {
	Location oldLoc = entity->getLocation();

	if (oldLoc.area_ == newLoc.area_) {
		if (isValidLocation(newLoc)) {
			tiles_[newLoc.x_][newLoc.y_].addEntity(entity);
			tiles_[oldLoc.x_][oldLoc.y_].removeEntity(entity);
			return newLoc;
		}
		return oldLoc;
	}
	else {
		oldLoc.area_->removeEntity(entity);
		newLoc.area_->addEntity(entity);
		return newLoc;
	}
}

std::vector<Entity*> Area::getEntitiesAt(Location target) {
	if (withinBounds(target)) {
		return tiles_[target.x_][target.y_].getEntities();
	}
	else {
		std::vector<Entity*> e;
		return e;
	}
}

std::vector<Entity*> Area::getEntitiesAdjacentTo(Location target) {
	if (withinBounds(target)) {

		std::vector<Entity*> entities;

		for (int i = -1; i < 2; i++) {
			for (int j = -1; j < 2; j++) {
				Location loc = Location(target.area_, target.x_ + i, target.y_ + j);
				if (target != loc) {
					std::vector<Entity*> e = getEntitiesAt(Location(target.area_, target.x_ + i, target.y_ + j));
					entities.reserve(entities.size() + e.size());
					entities.insert(entities.end(), e.begin(), e.end());
				}
			}
		}

		return entities;
	}
	else {
		std::vector<Entity*> e;
		return e;
	}
}

void Area::setFocusTracking(Observable* target) {
	focalPoint_.setTracking(target);
}

std::vector<std::vector<Tile>> Area::getTiles(int startX, int startY, int endX, int endY) {
    std::vector<std::vector<Tile>> result;
    result.reserve(endX - startX);

    for (int i = startX; i <= endX; i++) {
        result.push_back(std::vector<Tile>());
        result[i - startX].reserve(endY - startY);
        for (int j = startY; j <= endY; j++) {
            result[i - startX].push_back(tiles_[i][j]);
        }
    }

    return result;
}