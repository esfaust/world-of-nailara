#include "TargetingClickEntity.h"

#include "Logger.h"
#include "PromptEntityClicker.h"

TargetingClickEntity::TargetingClickEntity()
{
}


TargetingClickEntity::~TargetingClickEntity()
{
}


void TargetingClickEntity::target() {
	return;
}

bool TargetingClickEntity::hasValidTargets() {
	return true;
}

void TargetingClickEntity::applyEffect(Effect* effect) {
	if (!hasValidTargets())
		return;
	try {
		effect->apply(tempTarget_);
	}
	catch (Exception e) {
		typeid(effect).raw_name();
		Locator::getLogger()->log(SEV_INFO, std::string(typeid(this).name()) + " failed to apply effect '" + std::string(typeid(effect).name()));
	}
}

void TargetingClickEntity::promptHandler(Entity* chosenEntity) {
	tempTarget_ = chosenEntity;
	applyEffect(tempEffect_);
}

void TargetingClickEntity::targetAndApply(Effect* effect) {
	tempEffect_ = effect;
	std::function<void(Entity*)> callback = [this](Entity* e) { return this->promptHandler(e); };
	activePrompt_ = new PromptEntityClicker([this]() { return this->promptExitHandler(); }, callback);
}

void TargetingClickEntity::promptExitHandler() {
	delete activePrompt_;
}