#pragma once
#include "PromptWindow.h"

class PromptEntityClicker :
	public PromptWindow
{
public:
	PromptEntityClicker(std::function<void()> exitCallback, std::function<void(Entity*)> callback);
	void draw();
protected:
	
private:
	std::function<void(Entity*)> callback_;
	int cursorX_ = 0;
	int cursorY_ = 0;
	bool hasUpdated_ = false;

	void promptHandler(Location clickLoc);
	void promptHandler(Entity* ent);
	void promptExitHandler();
};

