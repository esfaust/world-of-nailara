#pragma once

#include <unordered_map>

#include "Logger.h"

// A class to log messages to a file
class LoggerFile :
	public Logger
{
public:
	LoggerFile(std::string filename, LoggingLevel level);
	~LoggerFile();

	// Logs the message to the file as well as console
	void log(LogSeverity severity, std::string msg);
private:
	// Logs the message to the file of the file logger
	void log_to_file(LogSeverity severity, std::string msg);
	// Retrieves the current time as a string
	std::string now();

	// Level of importance needed to log the event
	LoggingLevel level_;
	// The name of the file to store the log in
	std::string filename_;
	// The line or log entry number
	unsigned int logNumber_ = 0;
	// Whether or not to also log to the console
	bool logToConsole_ = true;
	
	// The severity levels as text in a map
	std::unordered_map<LogSeverity, std::string> severityText = severityMap();
	std::unordered_map<LogSeverity, std::string> severityMap() {
		std::unordered_map<LogSeverity, std::string> m;
		m[SEV_INFO] = "INFO";
		m[SEV_HEAVY] = "HEAVY";
		m[SEV_MEDIUM] = "MEDIUM";
		m[SEV_LIGHT] = "LIGHT";
		m[SEV_WARNING] = "WARNING";
		m[SEV_ERROR] = "ERROR";
		m[SEV_FATAL] = "FATAL";
		return m;
	}
};