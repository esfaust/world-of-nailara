#pragma once

#include <unordered_map>

class Sprite;
class Portrait
{
public:
    // There must be at least one default emotion
    Portrait(Sprite* sprite);
    ~Portrait();

    void add(std::string emotion, Sprite* sprite);
    void emotion(std::string emotion);

    int getWidth();
    int getHeight();

    void queue(int x, int y, int z);
    void queue(int x, int y, int z, int width, int height);

private:
    // <Emotion, Sprite*>
    std::unordered_map < std::string, Sprite*> sprites_;
    Sprite* currentEmotion_;
    Sprite* default_;
};

