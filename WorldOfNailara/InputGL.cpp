#include "InputGL.h"

#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/GLU.h>

// GLFW
#include <GLFW/glfw3.h>

#include "Locator.h"
#include "Logger.h"


void InputGL::key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
    Locator::getInput()->key_callback_impl(window, key, scancode, action, mode);
}

void InputGL::key_callback_impl(GLFWwindow* window, int key, int scancode, int action, int mode)
{

    last_.key = key;
    last_.action = action;
    last_.shift = !!(mode & 0x0001);
    last_.ctrl = !!(mode & 0x0002);
    last_.alt = !!(mode & 0x0004);

    available_ = true;
}

InputGL::InputGL(GLFWwindow *window)
{
    glfwSetKeyCallback((GLFWwindow*) window, this->key_callback);
}

InputGL::~InputGL()
{
}

// Reads an existing input event, or blocks until it receives one
void InputGL::read() {
    available_ = false;
    glfwPollEvents();
}
// Checks whether an input event is waiting in the channel
bool InputGL::hasInput() {
    return available_;
}
// Checks whether an input event is waiting, and returns the value without clearing it
InputEvent InputGL::check() {

    //Locator::getLogger()->log(SEV_INFO, "key pressed: " + std::to_string(last_.key));

    notify(EVENT_KEYBOARD, last_);
    last_ = InputEvent();
    available_ = false;
    return last_;
}