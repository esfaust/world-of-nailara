#pragma once

#include <memory>

class Entity;
class Area;

// This class is used to represent a physical place in the world
// 
class Location
{
public:
    Location();
	Location(Area *a, int ix, int iy) : area_(a), x_(ix), y_(iy) {};
	~Location() {};

	unsigned int x_, y_;
	Area *area_;

	float distance(Location t);
	void shift(int dx, int dy);

	friend bool operator==(Location const& lhs, Location const& rhs);
	friend bool operator!=(Location const& lhs, Location const& rhs);
};

