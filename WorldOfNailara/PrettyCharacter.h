#pragma once

#include "Color.h"
#include "StringRenderer.h"

#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/GLU.h>

// GLFW
#include <GLFW/glfw3.h>
#include <SOIL.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class Shader;

class PrettyCharacter
{
public:
    PrettyCharacter(Font* font, char ch);
    ~PrettyCharacter();

	void queue(int x, int y, int index=0);

	Character character_;
    GLuint textureID_;
    glm::ivec2 size_;
    glm::ivec2 bearing_;
    GLuint advance_;
    Color color_;
    Shader* shader_;
	Font* font_;
	char ch_;
	int index_;
};