﻿#pragma once

#include <string>
#include <vector>
#include <iostream>

#include "FocalPoint.h"
#include "Location.h"
#include "BorderedViewPort.h"
#include "GraphicalLayering.h"

class UiBase;
class Renderer;
class PromptTracker;
class Sprite;

#define ASCIIFONTWIDTH 8
#define ASCIIFONTHEIGHT 16

#define TILEWIDTH 64
#define TILEHEIGHT 64

/*
Interface class used to display things on the screen
*/
class Display
{
public:
	Display() {};
	Display(std::string name, int width, int height, int raWidth = 1, int raHeight = 1);
	// Renders the UI and the active area
	virtual void render();

	// Bridge functions - they simply call on to the components
	virtual void setLayer(int layer);
	virtual bool isInView(int mapX, int mapY);
	virtual void setFocalPoint(FocalPoint focalPoint);
	virtual void setActiveArea(Area* a);
	Area* getActiveArea();

	bool isAsciiMode() { return asciiMode_; }

	PromptTracker* getPrompts() { return prompts_; }
	std::unordered_map<std::string, int> getViewportInfo();
	bool mouseInViewPort(int mx, int my);

	int getHeight() { return height_; }
	int getWidth() { return width_; }

	Location translateViewPortCoordinate(int x, int y);

    Renderer* getRenderer() { return renderer_; }
    ViewPort* view() { return viewPort_; }

protected:
	PromptTracker* prompts_;
	Renderer* renderer_;
	ViewPort* viewPort_;
	UiBase* ui_;
	~Display();

	bool asciiMode_;
	int height_, width_;
	
};

// This is a null display used in case the locator is never provided a display to use
class NullDisplay : public Display
{
public:
	NullDisplay() {};
	NullDisplay(std::string name, int height, int width, int raWidth, int raHeight) {};
	void print(std::string tag, int x, int y, std::string color = "white") {};
	void printInRenderArea(std::string tag, int x, int y, std::string color = "white") {};
	void drawPrompt() {};
	void render() {};

	void setActiveArea(Area *a) {};
	void setFocalPoint(FocalPoint fp) {};
	bool isInRenderArea(int x, int y) { return false;  };
	void setLayer(int index) {};
};