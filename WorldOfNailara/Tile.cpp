#include "Tile.h"

#include <algorithm>
#include <random>

#include "Display.h"
#include "Locator.h"
#include "Sprite.h"

Tile::~Tile()
{
}


void Tile::render(int x, int y, int z) {
	//If this is not visible, exit render
	if (!Locator::getDisplay()->isInView(x_, y_))
		return;

	//Render the tile and all objects that exist on this tile
    appearance_->queue(x, y, z);
	for (unsigned int i = 0; i < entities_.size(); i++) {
        entities_[i]->render(x, y);
	}
	
}


void Tile::addEntity(Entity* e) {
	entities_.push_back(e);
}

void Tile::removeEntity(Entity* e) {
	entities_.erase(std::remove(entities_.begin(), entities_.end(), e), entities_.end());
}

std::vector<Entity*> Tile::getEntities() {
	return entities_;
}