#include "AiPlayer.h"

#include "Actor.h"
#include "Locator.h"
#include "PromptWindow.h"
#include "PromptEntityPicker.h"


#include "Ability.h"
#include "TargetingAdjacent.h"
#include "EffectSpeech.h"
#include "EffectInspect.h"
#include "TargetingClickEntity.h"
#include "Input.h"

#include <string>

AiPlayer::AiPlayer(Actor* owner) : AiBase(owner)
{
	Locator::getInput()->addObserver(this);
}


AiPlayer::~AiPlayer()
{
}

int AiPlayer::takeTurn() {
	if (waitingOnPrompt_) {
		return AP_;
	}

	if (AP_ < 0) {
		return AP_;
	}

	if (!waitingForInput_) {
		waitingForInput_ = true;
	}

	return AP_;
}

void AiPlayer::move(int dx, int dy) {
	AP_ -= 30;
	owner_->shift(dx, dy);
	owner_->notify(Event(EV_ENTITY_MOVEMENT), owner_->getLocation());
}

int AiPlayer::regenerateAP() {
	AP_ += 50;
	return AP_;
}


void AiPlayer::onNotify(Event event, InputEvent input) {
	if (!waitingForInput_)
		return;

	switch (input.key) {
	    case IK_UP:
            if(input.action == IK_PRESS)
		        move(0, -1);
		    break;
	    case IK_DOWN:
            if (input.action == IK_PRESS)
		        move(0, 1);
		    break;
	    case IK_LEFT:
            if (input.action == IK_PRESS)
		        move(-1, 0);
		    break;
	    case IK_RIGHT:
            if (input.action == IK_PRESS)
		        move(1, 0);
		    break;
	    case IK_T:
		    {
		    static Ability talk = Ability(new EffectSpeech("Hello, world!"), new TargetingAdjacent(owner_));
		    talk.use();
		    break;
		    }
	    case IK_I:
	    {
		    static Ability look = Ability(new EffectInspect(), new TargetingClickEntity());
		    look.use();
		    break;
	    }
        default:
            break;
	}
}

void AiPlayer::promptHandler(Entity* reaction) {
	delete activePrompt_;
}