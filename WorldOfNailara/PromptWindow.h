#pragma once

#include "Observer.h"
#include "Observable.h"

#include <functional>

class PromptWindow;

class PromptWindow
	: public Observer
{
public:
	~PromptWindow();
	virtual void draw();
protected:
	PromptWindow(std::function<void()> exitCallback);

	void promptExitHandler();
	std::function<void()> exitCallback_;
	PromptWindow* activePrompt_;
};