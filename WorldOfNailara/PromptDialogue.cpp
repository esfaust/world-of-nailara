#include "PromptDialogue.h"

#include "Portrait.h"
#include "Locator.h"
#include "Display.h"
#include "Renderer.h"
#include "Input.h"
#include "Texture.h"
#include "PrettyText.h"

#define PORTRAIT_SIZE 512
#define EDGEBUFFERX 32
#define EDGEBUFFERY 30


PromptDialogue::PromptDialogue(std::function<void()> exitCallback, Portrait* portrait, std::string text) : 
    PromptInformation(exitCallback, text), portrait_(portrait), shader_(Locator::getLibrary<Shader*>()->get<Shader*>("basic")), background_(Locator::getLibrary<Texture*>()->get<Texture*>("bg2")), border_(Locator::getLibrary<Texture*>()->get<Texture*>("borderh"))
{

    Display* disp = Locator::getDisplay();
    displayWidth_ = Locator::getDisplay()->getRenderer()->getWidth();
    displayHeight_ = Locator::getDisplay()->getRenderer()->getHeight();

    int borderWidth = Locator::getLibrary<Texture*>()->get<Texture*>("borderh")->getWidth();
    portraitX_ = displayWidth_ - PORTRAIT_SIZE - borderWidth;
    portraitY_ = displayHeight_ - PORTRAIT_SIZE - borderWidth;

    boxX_ = 0;
    boxY_ = displayHeight_ - int(PORTRAIT_SIZE * 0.75);
    boxWidth_ = displayWidth_ - PORTRAIT_SIZE;
    boxHeight_ = int(PORTRAIT_SIZE * 0.75);

    int lineWidth = boxWidth_ - borderWidth * 4;
    int lineHeight = boxHeight_ - borderWidth * 4;
	/*
	
	
	
	
	
	
	
	*/
	text = "<rgba=0,0,100,255>WOW!</rgba> You're so cool! <rgba=148,0,211,255>W</rgba><rgba=70,0,225,255>a</rgba><rgba=0,0,255,255>n</rgba><rgba=0,128,128,255>n</rgba><rgba=0,255,0,255>a b</rgba><rgba=255,255,0,255>e p</rgba><rgba=255,127,0,255>al</rgba><rgba=255,0,0,255>s?</rgba>";

	text_ = PrettyText(text, Locator::getLibrary<Font*>()->get<Font*>("arial-bold"), boxWidth_ - (2 * EDGEBUFFERX), boxHeight_ - (2 * EDGEBUFFERY));
}

PromptDialogue::~PromptDialogue()
{
}

void PromptDialogue::draw() {
    Locator::getDisplay()->getRenderer()->queueQuadBordered(boxX_, boxY_, GraphicalLayering::LAYER_PROMPT, boxWidth_, boxHeight_, shader_, background_, border_);
    
	text_.queue(boxX_ + EDGEBUFFERX, boxY_ + EDGEBUFFERY);

    portrait_->queue(portraitX_, portraitY_, GraphicalLayering::LAYER_PROMPT_PORTRAIT, PORTRAIT_SIZE, PORTRAIT_SIZE);
}

void PromptDialogue::onNotify(Event event, InputEvent input) {
    if (input.key == IK_ESCAPE)
        delete this;
}