#pragma once

#include "Drawable.h"

class EntityECS;
class Sprite;

/*
Visual
*/
class IVisual : public Drawable {
public:
    virtual void render() = 0;
    virtual void move(int x, int y) = 0; // Move visual to this location
protected:
    int x_ = 0, y_ = 0; // The screen location to display on
    int z_; // The layer to display on
    int width_ = 0, height_ = 0; // The size of the visual
};

// Visualizes an entity's body, as it has a location to 
class VisualBody : public IVisual {
public:
    void changeAppearance();
    void render();
    void move();
private:
    EntityECS* parent_; //The parent to use to determine where to display
    Sprite* appearance_;
};

class VisualNull : public IVisual {
public:
    void render() {};
    void move(int x, int y) {}; // Move visual to this location
};