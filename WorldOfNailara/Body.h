#pragma once
#include "Component.h"

#include "Location.h"
#include "BlockingFlags.h"
#include "Attributes.h"
#include "Form.h"

class Equipment;
class Form;
class DrawableBody;

/*
A Body is a thing that resides in the world somewhere. It may be incorporeal, ethereal and unable to affect things, but regardless, it has a location.
Bodies are defined by their Form - the description of what they look like. This could include a humanoid body with hands, limbs, organs, or a single mote like a Wisp.
Equipment is applied to a body
*/
class Body : public Attributes {
public:
	Body(Form* form);
    void shift(int dx, int dy); // Shifts the location of the body by the given numbers
    void teleport(int x, int y); // Teleports the body to the given location

    std::string getDescription(bool longDescription = false);

private:
    Location location_ = Location(NULL, 0, 0);
    Form* form_; // Forms are pointers so that the base layout can be shared - two orcs have the same layout until they are modified in some way.
    FormDescriptor descriptor_; // The descriptor set tied to this form and body, used to generate descriptions

    Equipment* equipment_; // Wielded or worn equipment, and inventory space and management.

	DrawableBody* visual_;
};
