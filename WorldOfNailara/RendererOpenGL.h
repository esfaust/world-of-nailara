﻿#pragma once
#include "Renderer.h"

#include <vector>
#include <map>
#include <cstdint>

#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/GLU.h>

// GLFW
#include <GLFW/glfw3.h>
#include <SOIL.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "RenderQueueItem.h"

class Sprite;
class SpriteSheet;
class StringRenderer;
class Font;

class RendererOpenGL :
	public Renderer
{
public:
	RendererOpenGL(std::string windowName, int height, int width);
	~RendererOpenGL();

	virtual void drawTag(std::string tag, int x, int y, std::string color = "white") {};
	virtual void setLayer(int index) {};
	
	// Renders the UI and the active area
	virtual void render();
    void clear();

	void init();

    void drawText(int x, int y, int z, std::string text, Font* font, float scale, Color color = Color());
	void drawCharacter(int x, int y, int z, char ch, Font* font, float scale, Color color = Color());

    StringRenderer* getStringRenderer();

	// Revisited functions/new functions


	// Draw a quad and automatically tile the texture
	void drawQuadTiling(int x, int y, int z, int width, int height, Shader* shader, Texture* texture, float rotation = 0.0f);
	// Draw a quad and resize the texture across the full quad
	void drawQuadFull(int x, int y, int z, int width, int height, Shader* shader, Texture* texture, float rotation=0.0f);
	// Draw a quad using the given texture coordinates
    void drawQuad(int x, int y, int z, int width, int height, Shader* shader, Texture* texture, float textureCoordinates[8], float rotation = 0.0f, Color color = Color(1.0f, 1.0f, 1.0f));

    void queueSpriteSheet(int x, int y, int z, int width, int height, SpriteSheet* sheet, int spriteIndex);
    void queueSprite(int x, int y, int z, Sprite* sprite);
    void queueSpriteResized(int x, int y, int z, int width, int height, Sprite* sprite);
    void queueQuad(int x, int y, int z, int width, int height, Shader* shader, Texture* texture, bool tiling = false);
    void queueQuadBordered(int x, int y, int z, int width, int height, Shader* shader, Texture* texture, Texture* border);
    void queueText(int x, int y, int z, std::string text, Font* font, float scale, Color color, Shader* shader);
	void queueCharacter(int x, int y, int z, char ch, Font* font, float scale, Color color, Shader* shader);

	void queuePrettyCharacter(int x, int y, int z, PrettyCharacter ch);

    // REMOVE THIS
    // Draw a quad bordered by another textured quad
    void drawQuadBordered(int x, int y, int z, int width, int height, Shader* shader, Texture* background, Texture* border, float rotation = 0.0f);

private:


    // Display the debug screens for testing all graphic functions
    void test();
    void testQueue();

    void fps();

    GLuint quadVAO_, quadVBO_, quadEBO_;
    GLFWwindow* window_;

    std::vector<float> qVertices_;
    std::vector<int> qIndices_;
    std::vector<RenderQueueItem> queue_;

    std::map<Shader*, std::map<Texture*, std::vector<RenderQueueItem>>> queueMap_;
    std::map<int, std::map<Shader*, std::map<Texture*, std::vector<RenderQueueItem>>>> queueMapZOrdered_;
    std::vector<RenderQueueItem> rqueue_;


    int indexCount_ = 0;
    
    StringRenderer* stringRenderer_;
    Shader* activeShader_;
    glm::mat4 projection_;

    double lastTime_ = 0;
    unsigned int nbFrames_ = 0;
    unsigned int lastFps_ = 0;

	Texture* debug_;
	Texture* error_;

	float vertices_[12] = {
		// Positions
		1.0f, 1.0f, 0.0f,	// Bottom Left
		1.0f, 0.0f, 0.0f,	// Bottom Right
		0.0f, 0.0f, 0.0f,	// Top Right
		0.0f, 1.0f, 0.0f	// Top Left
	};

	float fullQuadDraw_[8] = {
		0.0f, 0.0f
		, 0.0f, 1.0f
		, 1.0f, 1.0f
		, 1.0f, 0.0f
	};
};


/*
Texture Coords Refresher
1,1 (C)					0,1 (B)
____________________
|					 |
|					 |
|					 |
|					 |
|					 |
|					 |
|					 |
|____________________|
1,0 (D)					0,0 (A)

By default, the quad will be textured upside down.
Fixing this will be handled by the mat4 rotations and flipping
*/

// Entire Texture
//float tc[8] = {
//	0.0f, 0.0f		// Bottom Left of what we want to capture
//	, 0.0f, 1.0f    // Top Left of what we want to capture
//	, 1.0f, 1.0f    // Top Right of what we want to capture
//	, 1.0f, 0.0f	// Bottom Right of what we want to capture
//};

// Upper right quadrant
//float tc[8] = {
//	0.0f, 0.5f		// Bottom Left of what we want to capture
//	, 0.0, 1.0f    // Top Left of what we want to capture
//	, 0.5f, 1.0f    // Top Right of what we want to capture
//	, 0.5f, 0.5f	// Bottom Right of what we want to capture
//};