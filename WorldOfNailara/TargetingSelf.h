#pragma once
#include "Targeting.h"
#include "Effect.h"

class Entity;

class TargetingSelf :
	public Targeting
{
public:
	TargetingSelf(Entity* caller);
	~TargetingSelf();
	void target();
	bool hasValidTargets();
	void applyEffect(Effect* effect);
private:
	void promptExitHandler() {};

	Entity* caller_;
};

