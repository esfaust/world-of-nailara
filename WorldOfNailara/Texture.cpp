#include "Texture.h"

#include <SOIL.h>


Texture::Texture(std::string file) {
	std::string filepath = file;
	load(filepath);
}

Texture::Texture()
{
}

Texture::~Texture()
{
}

void Texture::load(std::string file) {
	glGenTextures(1, &ID_);
	glBindTexture(GL_TEXTURE_2D, ID_); // All upcoming GL_TEXTURE_2D operations now have effect on our texture object

	// Load, create texture and generate mipmaps
	unsigned char* image = SOIL_load_image(file.c_str(), &width_, &height_, 0, SOIL_LOAD_RGBA);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width_, height_, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
	glGenerateMipmap(GL_TEXTURE_2D);
	SOIL_free_image_data(image);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// Set texture wrapping to GL_REPEAT
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    // Set texture filtering
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glBindTexture(GL_TEXTURE_2D, 0);
}

void Texture::bind(int activeTexture) {
	switch (activeTexture) {
	case 0:
		glActiveTexture(GL_TEXTURE0);
		break;
	case 1:
		glActiveTexture(GL_TEXTURE1);
		break;
    default:
        glActiveTexture(GL_TEXTURE0);
        break;
	}
	
    glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, ID_);
}