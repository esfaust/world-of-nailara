#pragma once

#include <string>
#include <vector>
#include <unordered_map>

class Bodypart;

namespace BodypartConnector {
    enum Type {
        NONE,
        TORSO_NECK,
        TORSO_SHOULDER,
        TORSO_SKIN,
        HIP_LEG,
        ARM_WRIST,
        LEG_ANKLE,
        SKULL_EYE,
        SKULL_HORN,
        HIP_TAIL
    };
}

class BodypartConnection {
public:
    BodypartConnection(std::string name, Bodypart* bodypart) : name_(name), bodypart_(bodypart) {};
    BodypartConnection(std::string name, Bodypart* bodypart, BodypartConnection child);
    BodypartConnection(std::string name, Bodypart* bodypart, std::vector<BodypartConnection> children);

    bool connect(BodypartConnection part);
    bool disconnect(BodypartConnection part);
    BodypartConnector::Type connectorType();
    std::vector<BodypartConnection> getChildren();
    Bodypart* getBodypart();

    std::string getDescriptionTemplate();
private:
    std::string name_;
    Bodypart* bodypart_;
    std::vector<BodypartConnection> children_;
    std::unordered_map<BodypartConnector::Type, int> usedConnectors_; // How many connectors have been used of each type
};

