#include "Entity.h"

#include <iostream>

#include "Area.h"
#include "Display.h"
#include "Locator.h"
#include "Logger.h"
#include "Sprite.h"


Entity::Entity(std::string name, Sprite* ap, Location l) : appearance_(ap), loc_(l) {
	attr("name", name);
	attr("desc", "It's too blurry to tell what it is.");
}

Entity::Entity(std::string name, Sprite* ap, Location l, Blocking::Set blocking) : appearance_(ap), loc_(l), blocking_(blocking) {
	attr("name", name);
	attr("desc", "It's too blurry to tell what it is.");
}

Entity::~Entity() {
}


void Entity::render(int x, int y, int z) {
    appearance_->queue(x, y, z);
}

void Entity::update() {
}

void Entity::shift(int dx, int dy) {
	Location target = Location(loc_.area_, loc_.x_ + dx, loc_.y_ + dy);
	bool valid = loc_.area_->isValidLocation(target);
	if (!valid)
		return; // Throw exception
	if (FlagCalculation::isBlockedBy(blocking_, loc_.area_->getBlockingAt(target))) {
		return;
	}
	// --Start
	std::vector<Entity*> entities = loc_.area_->getEntitiesAt(target);
	for (auto ent : entities) {
		if (FlagCalculation::isBlockedBy(blocking_, ent->blocking_)) {
			return;
		}
	}
	// --End
	loc_ = loc_.area_->shiftEntity(this, dx, dy);
}

void Entity::teleport(Location l) {
	loc_ = l.area_->teleportEntity(this, l);
	loc_ = l;
}

template<typename T> void Entity::attr(std::string key, T value) {
	attributes_[key] = value;
}