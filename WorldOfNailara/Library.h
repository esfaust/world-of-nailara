#pragma once

#include <string>
#include <map>

template<typename T>
class Library
{
public:
    Library(T def) : default_(def) {}
    ~Library() {
    }

    T add(std::string key, T value) {
        lib_[key] = value;
		return value;
    }

    T get(std::string key) {
        if (lib_.count(key) != 0) {
            return (lib_[key]);
        }
        else {
            return default_;
        }
    }

    void setDefault(T def) {
        default_ = def;
    }

    T getDefault() {
        return default_;
    }

private:
    std::map<std::string, T> lib_;
    T default_;
};


#include <typeindex>
#include <unordered_map>
#include <boost\any.hpp>
#include "Logger.h"

class LibraryGeneric
{
public:
	template <typename T> LibraryGeneric(T def) : default_(def), index_(typeid(T)) {
	}

	template <typename T> T add(std::string key, T value) {
		lib_[key] = value;
		return value;
	}

	template <typename T> T get(std::string key) {
		if (std::type_index(typeid(T)) == index_) {
			if (lib_.count(key) != 0) {
				return boost::any_cast<T>(lib_[key]);
			}
			else {
				Locator::getLogger()->log(SEV_MEDIUM, "Returning default value from library");
				return boost::any_cast<T>(default_);
			}
		}
		else {
			Locator::getLogger()->log(SEV_FATAL, "Incorrect type requested from library!");
			return T();
		}
	}

	template <typename T> void setDefault(T def) {
		default_ = def;
	}

	template <typename T> T getDefault() {
		if (std::type_index(typeid(T)) == index_) {
			return boost::any_cast<T>(default_);
		}
		else {
			Locator::getLogger()->log(SEV_FATAL, "Incorrect type requested from library!");
			return T();
		}
	}

	std::type_index getIndex() {
		return index_;
	}

private:
	std::unordered_map<std::string, boost::any> lib_;
	boost::any default_;
	std::type_index index_;
};