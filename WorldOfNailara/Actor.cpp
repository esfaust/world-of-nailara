#include "Actor.h"

#include "AiPlayer.h"
#include "Display.h"
#include "Locator.h"

#include "Sprite.h"

Actor::Actor(std::string name, Sprite* appearance, Location location) : Entity(name, appearance, location) {
	personality_ = new AiPlayer(this);
	blocking_ = Blocking::S_Creature;
};

Actor::Actor(std::string name, Sprite* appearance, Location location, std::unique_ptr<AiBase> personality) : Entity(name, appearance, location) {
	personality_ = new AiPlayer(this);
	blocking_ = Blocking::S_Creature;
};

Actor::~Actor() {
	delete personality_;
}

int Actor::takeTurn() {
	return personality_->takeTurn();
}


int Actor::regenerateAP() {
	return personality_->regenerateAP();
}


void Actor::render() {
	Locator::getDisplay()->setLayer(GraphicalLayering::LAYER_ACTORS);
	appearance_->draw(11, 11,0);
}

void Actor::update() {
	personality_->takeTurn();
	personality_->regenerateAP();
}

void Actor::setPersonality(AiBase* personality) {
	personality_ = personality;
}