#include "BodypartConnection.h"

#include <vector>

#include "Locator.h"
#include "Logger.h"
#include "Body.h"
#include "Bodypart.h"


BodypartConnection::BodypartConnection(std::string name, Bodypart* bodypart, BodypartConnection child) : name_(name), bodypart_(bodypart) {
    if (!connect(child)) {
        Locator::getLogger()->log(SEV_MEDIUM, "Failed to connect bodypart.");
    }
}

BodypartConnection::BodypartConnection(std::string name, Bodypart* bodypart, std::vector<BodypartConnection> children) : name_(name), bodypart_(bodypart) {
    for (auto child : children) {
        if (!connect(child)) {
            Locator::getLogger()->log(SEV_MEDIUM, "Failed to connect bodypart.");
        }
    }
}

BodypartConnector::Type BodypartConnection::connectorType() {
    return bodypart_->connectorType(); 
}

bool BodypartConnection::connect(BodypartConnection part) {
    BodypartConnector::Type type = part.connectorType();
    int openSpots = bodypart_->connectorCount(type);
    int spotsUsed = usedConnectors_[type];

    if (spotsUsed < openSpots) {
        children_.push_back(part);
        usedConnectors_[type] = usedConnectors_[type]++;
        return true;
    }
    Locator::getLogger()->log(SEV_MEDIUM, "Failed to connect bodypart '" + part.name_ + "' to base '" + name_ + "'");
    return false;
}

bool BodypartConnection::disconnect(BodypartConnection part) {

    Locator::getLogger()->log(SEV_MEDIUM, "Failed to disconnect bodypart '" + part.name_ + "' from base '" + name_ + "'");
    return false;
}

Bodypart* BodypartConnection::getBodypart() {
    return bodypart_;
}

std::vector<BodypartConnection> BodypartConnection::getChildren() {
    return children_;
}

std::string BodypartConnection::getDescriptionTemplate() {
    std::string descTemplate = bodypart_->getDescriptionTemplate();
    for (auto child : children_) {
        std::string childTemplate = child.getDescriptionTemplate();
        if (childTemplate != "") {
            if (descTemplate == "") {
                descTemplate = child.getDescriptionTemplate();
            }
            else {
                descTemplate = descTemplate + " " + child.getDescriptionTemplate();
            }
        }
    }
    return descTemplate;
}










