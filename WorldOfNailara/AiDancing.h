#pragma once
#include "AiBase.h"

class Actor;

// A testing AI that moves back and forth forever
class AiDancing :
	public AiBase
{
public:
	AiDancing(Actor* owner) : AiBase(owner) {}
	// Inherited from BaseAi, now simply moves back and forth
	int takeTurn();
	// Inherited from BaseAi
	int regenerateAP();
};

