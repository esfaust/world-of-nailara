#pragma once

#include "Color.h"

class Renderer;
class Sprite;
class Texture;
class Shader;

class RenderItem {
public:
	virtual void draw() = 0;

	virtual Texture* getTexture()=0;
	virtual Shader* getShader() { return shader_; }
	int getIndex() { return index_; }
	void setIndex(int index) { index_ = index; }

	int x() { return x_; }
	int y() { return y_; }
	int getZ() { return z_; }
	void x(int x) { x_ = x; }
	void y(int y) { y_ = y; }
	void z(int z) { z_ = z; }

protected:
	int x_, y_, z_;
	int index_;
	Shader* shader_;
};


class RenderItemSprite : public RenderItem {
public:
	RenderItemSprite(int x, int y, int z, Shader* shader, Sprite* sprite);

	void draw();

	Texture* getTexture();
private:
	Sprite* sprite_;
};

class RenderItemQuad : public RenderItem {
public:
	void draw();

	Color color_;
};