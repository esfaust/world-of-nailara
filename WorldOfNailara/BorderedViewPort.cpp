#include "BorderedViewPort.h"

#include "Display.h"
#include "Locator.h"
#include "Exception.h"
#include "Renderer.h"
#include "Tile.h"

BorderedViewPort::BorderedViewPort(int x, int y, int width, int height) : ViewPort(x,y,width,height) {
    focalPoint_ = new FocalPoint(2, 2);
    borderWidth_ = 16;
    
    interiorWidth_ = width - borderWidth_ * 2;
    interiorHeight_ = height - borderWidth_ * 2;
    
    fitTilesHorizontally = interiorWidth_ / TILEWIDTH;
    fitTilesVertically = interiorHeight_ / TILEHEIGHT;
    evenX_ = (fitTilesHorizontally % 2) == 0;
    evenY_ = (fitTilesVertically % 2) == 0;
    paddingX_ = width_ / 2 / TILEWIDTH;
    paddingY_ = height_ / 2 / TILEHEIGHT;
    
    offsetX_ = x;
    offsetY_ = y;
}

void BorderedViewPort::draw() {
    // Determine what tiles are around the view point
    int startX, endX, startY, endY;
    startX = std::max(focalPoint_->x_ - paddingX_, 0);
    endX = std::min(focalPoint_->x_ + paddingX_ - evenX_, (int)activeArea_->getWidth() - 1);
    startY = std::max(focalPoint_->y_ - paddingY_, 0);
    endY = std::min(focalPoint_->y_ + paddingY_ - evenY_, (int)activeArea_->getHeight() - 1);
    std::vector<std::vector<Tile>> tiles = activeArea_->getTiles(startX, startY, endX, endY);

    // Shifting if player is at the edge of the map
    int adjustmentX = std::abs(std::min(focalPoint_->x_ - paddingX_, 0));
    int adjustmentY = std::abs(std::min(focalPoint_->y_ - paddingY_, 0));

    int fw = fitTilesHorizontally * TILEWIDTH;
    int fh = fitTilesVertically * TILEHEIGHT;

    Locator::getDisplay()->getRenderer()->queueQuadBordered(offsetX_, offsetY_, GraphicalLayering::LAYER_VIEW_PORT, fw + borderWidth_ * 2, fh + borderWidth_ * 2, Locator::getLibrary<Shader*>()->get<Shader*>("basic"), Locator::getLibrary<Texture*>()->get<Texture*>("bg"), Locator::getLibrary<Texture*>()->get<Texture*>("borderh"));
    for (int x = 0; x <= endX - startX; ++x) {
        for (int y = 0; y <= endY - startY; ++y) {
            tiles[x][y].render(offsetX_ + borderWidth_ + (x + adjustmentX) * TILEWIDTH, offsetY_ + borderWidth_ + (y + adjustmentY) * TILEHEIGHT);
        }
    }
}