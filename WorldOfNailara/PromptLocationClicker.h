#pragma once
#include "PromptWindow.h"
class PromptLocationClicker :
	public PromptWindow
{
public:
	PromptLocationClicker(std::function<void()> exitCallback, std::function<void(Location)> callback);

	void onNotify(Event event, InputEvent input);
	void draw();
private:
	std::function<void(Location)> callback_;
	int cursorX_ = 0;
	int cursorY_ = 0;
	bool hasUpdated_ = false;
};

