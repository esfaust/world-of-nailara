#pragma once

#include "InputEvent.h"
#include "Observable.h"
#include "PromptTracker.h"
#include "InputDefs.h"

struct GLFWwindow;

// This interface class handles gathering inputs and translating them into the more easy to use InputEvent
// Observers can register interest in input events, but will need to parse whether they care about the ones thrown on their own
class Input : public Observable
{
public:
	// Reads an existing input event, or blocks until it receives one
    virtual void read() = 0;
	// Checks whether an input event is waiting in the channel
    virtual bool hasInput() = 0;
	// Checks whether an input event is waiting, and returns the value without clearing it
    virtual InputEvent check() = 0;

	PromptTracker* getPrompts() { return prompts_; }

	void notify(Event event, InputEvent input) {
		if (prompts_->isEmpty()) {
			for (unsigned int i = 0; i < observers_.size(); ++i) {
				observers_[i]->onNotify(event, input);
			}
		}
		else {
			prompts_->getActivePrompt()->onNotify(event, input);
		}
	}

    virtual void key_callback_impl(GLFWwindow* window, int key, int scancode, int action, int mode) {}

protected:
	PromptTracker* prompts_ = new PromptTracker();
};

// A null input gatherer, in case the locator is never provided an input instance.
// It only ever returns blank inputs.
class NullInput : public Input {
public:
	NullInput() {};
	void read() { };
	bool hasInput() { return false; };
	InputEvent check() { return InputEvent(); }
};