#include "Renderer.h"

#include "Texture.h"
#include "Shader.h"
#include "Locator.h"
#include "Logger.h"

void Renderer::add(RenderItem* item) {
	// TODO: This may be inaccurate for setting highest used when the queue fills
	if (!filledQueue_) {
		itemQueue_[queueLowestOpen_] = item;
		int index = queueLowestOpen_;
		if (queueLowestOpen_ > queueHighestUsed_) {
			// Everything behind the lowest open spot is filled
			queueHighestUsed_++;
			queueLowestOpen_++; // Next slot must be open
			if (queueLowestOpen_ == MAX_QUEUE_ITEMS && queueHighestUsed_ + 1 == MAX_QUEUE_ITEMS) {
				// Entire queue is filled
				filledQueue_ = true;
			}
		}
		else {
			// We're refilling a previously used slot
			for (int i = queueLowestOpen_ + 1; i < MAX_QUEUE_ITEMS; i++) {
				// Search forward for an open spot
				if (itemQueue_[i] == NULL) {
					queueLowestOpen_ = i;
				}
			}
			filledQueue_ = true;
		}
	}
	//Locator::getLogger();
	//std::sort(std::begin(itemQueue_), std::end(itemQueue_));
	std::sort(std::begin(itemQueue_), std::end(itemQueue_), comp);
	//Locator::getLogger();

	for (int i = 0; i < MAX_QUEUE_ITEMS; i++) {
		if (itemQueue_[i] == NULL) {
			break;
		}
		else {
			itemQueue_[i]->setIndex(i);
		}
	}


	filledQueue_ = filledQueue_;
	return;
}

void Renderer::destroy(RenderItem* data) {

	int index = data->getIndex();
	if (index < MAX_QUEUE_ITEMS && index >= 0) {
		if (itemQueue_[index] != NULL) {
			if (itemQueue_[index]->getIndex() == index) {
				itemQueue_[index] = NULL;
			}
		}
	}
	Locator::getLogger();
	std::sort(std::begin(itemQueue_), std::end(itemQueue_), comp);
	Locator::getLogger();

	for (int i = 0; i < MAX_QUEUE_ITEMS; i++) {
		if (itemQueue_[i] == NULL) {
			break;
		}
		else {
			itemQueue_[i]->setIndex(i);
		}
	}

	
	return;
}

bool Renderer::comp(RenderItem *lhs, RenderItem *rhs) {
	if (lhs == NULL || lhs == rhs) {
		return false;
	}

	if (rhs == NULL) {
		return true;
	}

	if (lhs->getTexture()->ID_ >= rhs->getTexture()->ID_) {
		if (lhs->getShader()->Program >= rhs->getShader()->Program) {
			if (lhs->getZ() < rhs->getZ()) {
				return true;
			}
		}
		else if (lhs->getTexture()->ID_ > rhs->getTexture()->ID_) {
			return true;
		}
	}
	else if (lhs->getTexture()->ID_ > rhs->getTexture()->ID_) {
		return true;
	}

	return false;
}