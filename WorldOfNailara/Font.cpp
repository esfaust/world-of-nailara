#include "Font.h"

#include <iostream>
#include <fstream>
#include <sstream>

#include "Locator.h"
#include "Logger.h"
#include "Shader.h"

#include <ft2build.h>
#include FT_FREETYPE_H

Font::Font(std::string name)
{
	std::string fontfile = "Fonts\\" + name;

    texture_ = new Texture(fontfile + ".png");
    Locator::getLibrary<Texture*>()->add(name, texture_);
    std::string filename = fontfile + ".fnt";
    std::ifstream stream(filename.c_str());
    if (!stream.good()) {
		Locator::getLogger()->log(SEV_HEAVY, "Error reading font '" + filename + "'");
        return;
    }

    std::string line;
    std::string read, key, value;
    std::size_t i;
    while (!stream.eof())
    {
        std::getline(stream, line);
        std::stringstream lineStream;
        lineStream << line;

        // read the line's type
        lineStream >> read;
        if (read == "common")
        {
            //this holds common data
            while (!lineStream.eof())
            {
                std::stringstream Converter;
                lineStream >> read;
                i = read.find('=');
                key = read.substr(0, i);
                value = read.substr(i + 1);

                //assign the correct value
                Converter << value;
                if (key == "lineHeight")
                    Converter >> charsetDesc_.lineHeight_;
                else if (key == "base")
                    Converter >> charsetDesc_.base_;
                else if (key == "scaleW")
                    Converter >> charsetDesc_.width_;
                else if (key == "scaleH")
                    Converter >> charsetDesc_.height_;
                else if (key == "pages")
                    Converter >> charsetDesc_.pages_;
            }
        }
        else if (read == "char")
        {
            //this is data for a specific char
            unsigned short CharID = 0;

            while (!lineStream.eof())
            {
                std::stringstream Converter;
                lineStream >> read;
                i = read.find('=');
                key = read.substr(0, i);
                value = read.substr(i + 1);

                //assign the correct value
                Converter << value;
				if (key == "id") {
					Converter >> CharID;
					//charsetDesc_.Chars[CharID].char_ = 'c';
				}
                else if (key == "x")
                    Converter >> charsetDesc_.Chars[CharID].x_;
                else if (key == "y")
                    Converter >> charsetDesc_.Chars[CharID].y_;
                else if (key == "width")
                    Converter >> charsetDesc_.Chars[CharID].size_.x;
                else if (key == "height")
                    Converter >> charsetDesc_.Chars[CharID].size_.y;
                else if (key == "xoffset")
                    Converter >> charsetDesc_.Chars[CharID].xOffset_;
                else if (key == "yoffset")
                    Converter >> charsetDesc_.Chars[CharID].yOffset_;
                else if (key == "xadvance")
                    Converter >> charsetDesc_.Chars[CharID].advance_;
                else if (key == "page")
                    Converter >> charsetDesc_.Chars[CharID].page_;
            }
        }
    }

    // Load texturing
    loadTexture();
}

void Font::loadTexture() {
    for (int i = 0; i < 256; i++) {
        Character ch = charsetDesc_.Chars[i];
        ch.tx = ch.x_ / float(texture_->getWidth());
        ch.ty = ch.y_ / float(texture_->getHeight());
        ch.tw = ch.size_.x / float(texture_->getWidth());
        ch.th = ch.size_.y / float(texture_->getHeight());
		ch.char_ = i;
        charsetDesc_.Chars[i] = ch;
    }
    textureID_ = texture_->ID_;
}

Font::~Font()
{
}

Character Font::character(int ch) {
    return charsetDesc_.Chars[ch];
}