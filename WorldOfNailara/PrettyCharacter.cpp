#include "PrettyCharacter.h"

#include "Locator.h"
#include "StringRenderer.h"

#include "Locator.h"
#include "Display.h"
#include "Renderer.h"

PrettyCharacter::PrettyCharacter(Font* font, char c)
{
	Character ch = font->character(c);
	font_ = font;
    size_ = ch.size_;
    bearing_ = ch.bearing_;
    advance_ = ch.advance_;
    color_ = Color();
    shader_ = Locator::getLibrary<Shader*>()->get<Shader*>("text");
	ch_ = ch.char_;
}


PrettyCharacter::~PrettyCharacter()
{
}

void PrettyCharacter::queue(int x, int y, int index) {
	Locator::getDisplay()->getRenderer()->queueCharacter(x, y, GraphicalLayering::LAYER_PROMPT_TEXT, ch_, Locator::getLibrary<Font*>()->get<Font*>("arial-bold"), 1.0f, color_, Locator::getLibrary<Shader*>()->get<Shader*>("text"));
}
