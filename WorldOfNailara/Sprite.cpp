#include "Sprite.h"

#include <vector>

#include "SpriteSheet.h"
#include "RenderItem.h"
#include "Locator.h"
#include "Display.h"
#include "Renderer.h"


Sprite::Sprite(SpriteSheet* sheet, int index) : index_(index), sheet_(sheet) {
}

Sprite::Sprite()
{
}


Sprite::~Sprite()
{
}

void Sprite::draw(int x, int y, int z) {
    sheet_->draw(x,y,z,index_);
}

void Sprite::draw(int x, int y, int z, int width, int height) {
    sheet_->draw(x, y, z, index_, width, height);
}

void Sprite::queue(int x, int y, int z) {
    sheet_->queue(index_,x, y, z);
}

void Sprite::queue(int posX, int posY, int z, int width, int height) {
    sheet_->queue(index_, posX, posY, z, width, height);
}

int Sprite::getHeight() {
    return sheet_->getSpriteHeight();
}

int Sprite::getWidth() {
    return sheet_->getSpriteWidth();
}

Texture* Sprite::getTexture() {
	return sheet_->getTexture();
}