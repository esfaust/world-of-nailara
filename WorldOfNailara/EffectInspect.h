#pragma once
#include "Effect.h"

class PromptWindow;

// Inspects an entity to get its description back, as a prompt window
class EffectInspect :
	public Effect
{
public:
	EffectInspect();
	~EffectInspect();

	void apply(Entity* target);
private:
	void promptExitHandler();
	PromptWindow* activePrompt_;
};

