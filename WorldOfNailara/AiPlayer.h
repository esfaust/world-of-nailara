#pragma once

#include <memory>

#include "AiBase.h"
#include "Observer.h"

#include <string>

class PromptWindow;

// An AI for a controllable player
class AiPlayer :
	public AiBase, public Observer
{
public:
	AiPlayer(Actor* owner);
	~AiPlayer();

	// Takes a turn, like input and stuff?
	int takeTurn();
	// Regenerates AP by speed. So you can do more things!!!
	int regenerateAP();
	// MOVES THE PLAYER! Players love movment
	void move(int dx, int dy);

	// Reacts to input events by calling movement if its their turn
	virtual void onNotify(Event event, InputEvent input);

	void promptHandler(Entity* reaction);

	//void useAbility(Ability ability);

private:
	
	bool waitingForInput_ = false; // Is it the players turn? If so, we want that tasty input
	bool waitingOnPrompt_ = false; // Whether or not the player's input should be locked so they aren't using abilities while typing
	PromptWindow* activePrompt_; // The prompt we are waiting on
};

