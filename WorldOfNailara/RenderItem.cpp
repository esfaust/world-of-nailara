#include "RenderItem.h"

#include "Renderer.h"
#include "Sprite.h"


RenderItemSprite:: RenderItemSprite(int x, int y, int z, Shader* shader, Sprite* sprite) {
	x_ = x;
	y_ = y;
	z_ = z;
	shader_ = shader;
	sprite_ = sprite;
}

void RenderItemSprite::draw() {
	sprite_->draw(x_, y_, z_);
}

Texture* RenderItemSprite::getTexture() {
	return sprite_->getTexture();
}