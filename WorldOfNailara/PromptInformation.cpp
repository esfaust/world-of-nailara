﻿#include "PromptInformation.h"

#include "Locator.h"
#include "Input.h"
#include "Locator.h"
#include "Display.h"
#include "Renderer.h"
#include "Library.h"
#include "Sprite.h"
#include "GraphicalLayering.h"

PromptInformation::PromptInformation(std::function<void()> exitCallback, std::string text) : PromptWindow(exitCallback), text_(text)
{
    Display* disp = Locator::getDisplay();
    int w = Locator::getDisplay()->getRenderer()->getWidth();
    int h = Locator::getDisplay()->getRenderer()->getHeight();

    x_ = int(w / 7);
    width_ = int(x_ * 5);
    y_ = int(h / 7);
    height_ = int(y_ * 5);
}


PromptInformation::~PromptInformation()
{
}

void PromptInformation::draw() {
    Locator::getDisplay()->getRenderer()->queueQuadBordered(x_, y_, GraphicalLayering::LAYER_PROMPT, width_, height_, Locator::getLibrary<Shader*>()->get<Shader*>("basic"), Locator::getLibrary<Texture*>()->get<Texture*>("bg2"), Locator::getLibrary<Texture*>()->get<Texture*>("borderh"));
    Locator::getDisplay()->getRenderer()->queueText(x_ + 16, y_ + 16, GraphicalLayering::LAYER_PROMPT, text_, Locator::getLibrary<Font*>()->get<Font*>("arial-bold"), 0.5f, Color(1.0f, 1.0f, 1.0f), Locator::getLibrary<Shader*>()->get<Shader*>("text"));

    Locator::getDisplay()->getRenderer()->queueSpriteResized(x_ + width_ - 256 - 16, y_ + height_ - 256 - 16, GraphicalLayering::LAYER_PROMPT, 256, 256, Locator::getLibrary<Sprite*>()->get<Sprite*>("kona"));
}

void PromptInformation::onNotify(Event event, InputEvent input) {
	if (input.key == IK_ESCAPE)
		delete this;
}