#pragma once

#include "Area.h"
#include "FocalPoint.h"
#include "Observer.h"

class Sprite;

class ViewPort : public Observer
{
public:
	ViewPort(int x, int y, int width, int height);

	// Returns whether or not a given map coordinate is within view
	bool isInRenderArea(int mapX, int mapY);

	// Sets the area that should be rendered
	void setActiveArea(Area *area);

	// Sets the focal point of the map
	void focalTracking(Entity* ent);

	int translateX(int mapX);
	int translateY(int mapY);

	const int getWidth() { return width_; }
	const int getHeight() { return height_; }
	const int getOffsetX() { return offsetX_; }
	const int getOffsetY() { return offsetY_; }
	std::unordered_map<std::string, int> getInfo();
	Area* getArea();

	Location translateViewPortCoordinate(int x, int y);

	virtual void draw();
	bool mouseInViewPort(int mx, int my);

protected:
	Area* activeArea_;
	FocalPoint* focalPoint_;

	// Where to place the viewport into the map, referencing true x/y grid, rather than tiles
	int width_, height_;
	int offsetX_, offsetY_;
	int paddingX_, paddingY_;
	int centerX_, centerY_;
	bool evenX_=false, evenY_=false;

    int fitTilesHorizontally = 0, fitTilesVertically = 0;

	bool asciiMode_;	
};

