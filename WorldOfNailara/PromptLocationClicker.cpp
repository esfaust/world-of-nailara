﻿#include "PromptLocationClicker.h"

#include "Display.h"
#include "Locator.h"
#include "Exception.h"
#include "Input.h"

PromptLocationClicker::PromptLocationClicker(std::function<void()> exitCallback, std::function<void(Location)> callback) : PromptWindow(exitCallback), callback_(callback)
{
}

void PromptLocationClicker::draw() {
	if (hasUpdated_) {
		Display* disp = Locator::getDisplay();

		std::unordered_map<std::string, int> info = disp->getViewportInfo();
		int offsetX_ = info["offsetX"];
		int offsetY_ = info["offsetY"];

		int rx = (cursorX_ - offsetX_) % 4;
		int ry = (cursorY_ - offsetY_) % 2;

		int mx = (int)std::floor((cursorX_ - offsetX_) / 4);
		int my = (int)std::floor((cursorY_ - offsetY_) / 2);

		//Locator::getDisplay()->drawAppearance(Locator::getAppearanceLibrary()->getResource("cursor"), cursorX_ - rx, cursorY_ - ry, true);
	}
}

void PromptLocationClicker::onNotify(Event event, InputEvent input) {
	if (input.key == IK_ESCAPE) {
		Locator::getLogger()->log(SEV_INFO, "Exit callbacking");
		exitCallback_();
		return;
	}

	
	/*if (input.key == IM_MOUSE_MOVE) {
		Display* disp = Locator::getDisplay();
		if (disp->mouseInViewPort(input.mx, input.my)) {
			cursorX_ = input.mx;
			cursorY_ = input.my;
			hasUpdated_ = true;
		}
	}*/
	if (hasUpdated_) {
		if (input.lmb) {
			Display* disp = Locator::getDisplay();
			std::unordered_map<std::string, int> info = disp->getViewportInfo();
			int offsetX_ = info["offsetX"];
			int offsetY_ = info["offsetY"];

			Location loc = disp->translateViewPortCoordinate((int)std::floor((cursorX_ - offsetX_) / (TILEWIDTH / ASCIIFONTWIDTH)), (int)std::floor((cursorY_ - offsetY_) / (TILEHEIGHT / ASCIIFONTHEIGHT)));
			callback_(loc);
		}
	}
}