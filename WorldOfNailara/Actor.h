#pragma once

#include <unordered_map>
#include <memory>

#include "AiBase.h"
#include "Entity.h"
#include "Location.h"

// TODO: Change to a component system rather than subclasses

// An entity subclass that has a personality, and can move itself and take actions
class Actor :
	public Entity
{
public:
	// Constructor requires an appearance for rendering, and a location to place it on the map
	// Future definitions of the actor class might have location be optional
	// Those actors would exist in the void, and potentially be used as prototypes for spawners
	Actor(std::string name, Sprite* appearance, Location location);
	Actor(std::string name, Sprite* appearance, Location location, std::unique_ptr<AiBase> personality);
	~Actor();

	// Makes the actor check its AP and take a turn if it is ready to do so
	int takeTurn();
	// Refreshes the actor's AP by an amount equal to its speed
	int regenerateAP();

	// Renders the actor if it is on screen
	void render();
	// Attempts to take a turn, and regenerates AP
	void update();

	void setPersonality(AiBase* personality);

	// The personality that determines how the actor reacts to the world around them
	AiBase* personality_ = new AiNull();

	// Flags such as "INCORPOREAL" or other effects that may change how the actor interacts with the world
	std::unordered_map<std::string, bool> flags_;
};