#include "FocalPoint.h"

#include "Locator.h"
#include "Observable.h"
#include "Logger.h"

 FocalPoint::FocalPoint(int x, int y) : x_(x), y_(y) {
};

 void FocalPoint::setTracking(Observable* target) {
     Locator::getLogger()->log(SEV_INFO, "Tracking target.");
	 if(trackingTarget_)
		trackingTarget_->removeObserver(this);
	 target->addObserver(this);
	 trackingTarget_ = target;
 }

 void FocalPoint::onNotify(Event event, Location location) {
     //Locator::getLogger()->log(SEV_INFO, "Location change: " + std::to_string(location.x_) + "," + std::to_string(location.y_));
	 x_ = location.x_;
     y_ = location.y_;

 }