#pragma once

#include <string>

#include "Color.h"
#include "SpriteSheet.h"

class Texture;
class Shader;
class Renderer;
class Font;


class RenderQueueItem {
public:
    RenderQueueItem(int x, int y, int z, int width, int height, Shader* shader, Texture* texture, bool tiling = false)  // Quad
        : posX_(x), posY_(y), z_(z), width_(width), height_(height), shader_(shader), texture_(texture), type_((tiling ? RQ_QUADTILING : RQ_QUAD)) {}
    RenderQueueItem(int x, int y, int z, int width, int height, Shader* shader, Texture* texture, Texture* border)      // Bordered Quad
        : posX_(x), posY_(y), z_(z), width_(width), height_(height), shader_(shader), texture_(texture), border_(border), type_(RQ_QUADBORDERED) {}
    RenderQueueItem(int x, int y, int z, int width, int height, SpriteSheet* sheet, int index);                         // Sprite
    RenderQueueItem(int x, int y, int z, std::string text, Font* font, float scale, Color color, Shader* shader);       // Text
	RenderQueueItem(int x, int y, int z, char ch, Font* font, float scale, Color color, Shader* shader);       // Char

    void draw(Renderer* renderer);

    SpriteSheet* sheet_;
    Texture* texture_;
    Texture* border_;
    Shader* shader_;
    int posX_, posY_, z_;
    int width_, height_;
    int index_;
    float scale_;
    Color color_;
    std::string text_;
    Font* font_;
	char char_;

    enum RenderQueueType {
        RQ_UNDEFINED,
        RQ_SPRITE,
        RQ_QUAD,
        RQ_QUADTILING,
        RQ_QUADBORDERED,
        RQ_TEXT,
		RQ_CHAR
    };
    RenderQueueType type_ = RQ_UNDEFINED;

    bool operator < (const RenderQueueItem& other) const
    {
        return z_ < other.z_;


        /*if (shader_ == other.shader_) {
            if (z_ == other.z_) {
                return true;
            }
            else {
                return (z_ < other.z_);
            }
        }
        else {
            return (shader_ < other.shader_);
        }*/
    }
};