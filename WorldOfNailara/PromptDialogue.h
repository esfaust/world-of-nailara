#pragma once
#include "PromptInformation.h"

#include "PrettyText.h"

class Shader;
class Texture;
class Portrait;

class PromptDialogue :
    public PromptInformation
{
public:
    PromptDialogue(std::function<void()> exitCallback, Portrait* portrait, std::string text);
    ~PromptDialogue();

    void draw();
    void onNotify(Event event, InputEvent input);

	PrettyText text_;

private:
    Portrait* portrait_;

    int displayWidth_, displayHeight_;
    int portraitX_, portraitY_;
    int boxX_, boxY_;
    int boxWidth_, boxHeight_;

    Shader* shader_;
    Texture* background_;
    Texture* border_;
};

