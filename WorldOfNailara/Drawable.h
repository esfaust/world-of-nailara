#pragma once

class RenderItem;
class Sprite;
class Shader;
class Body;

class Drawable {
public:
	virtual void queue(int x, int y, int z = 0) = 0;
	void dequeue();

	~Drawable();

protected:
	int index_; // Index where it is stored in renderer
	RenderItem *queueData_; // Where the data is stored
};


class DrawableBody : public Drawable {
public:
	DrawableBody(Body* parent, Sprite* sprite);
	void queue(int x, int y, int z = 0);
private:
	Sprite* sprite_;
	Shader* shader_;
	Body* parent_;
};



class ExampleDrawable : public Drawable {
public:
	ExampleDrawable(Sprite* sprite, Shader* shader);
	void queue(int x, int y, int z = 0);
private:
	Sprite* sprite_;
	Shader* shader_;
};