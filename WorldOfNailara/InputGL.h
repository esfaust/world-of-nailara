#pragma once
#include "Input.h"

#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/GLU.h>

// GLFW
#include <GLFW/glfw3.h>

class InputGL :
    public Input
{
public:
    // Reads an existing input event, or blocks until it receives one
    void read();
    // Checks whether an input event is waiting in the channel
    bool hasInput();
    // Checks whether an input event is waiting, and returns the value without clearing it
    InputEvent check();
    InputGL(GLFWwindow *window);
    ~InputGL();
private:
    InputEvent last_;
    bool available_ = false;

    static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
    void key_callback_impl(GLFWwindow* window, int key, int scancode, int action, int mode);

};
