#include "Bodypart.h"

int Bodypart::connectorCount(BodypartConnector::Type type) {
    if (!connectors_.count(type))
        return 0;

    return connectors_[type];
}

BodypartConnector::Type Bodypart::connectorType() {
    return connectorType_;
}

std::string Bodypart::getDescriptionTemplate() {
    return description_;
}