#include "RenderQueueItem.h"

#include "Renderer.h"
#include "SpriteSheet.h"
#include "Display.h"
#include "Shader.h"
#include "Font.h"


RenderQueueItem::RenderQueueItem(int x, int y, int z, std::string text, Font* font, float scale, Color color, Shader* shader)
    : posX_(x), posY_(y), z_(z), text_(text), font_(font), scale_(scale), color_(color), shader_(shader), type_(RQ_TEXT) {
    texture_ = font->getTexture();
}

RenderQueueItem::RenderQueueItem(int x, int y, int z, char ch, Font* font, float scale, Color color, Shader* shader)
	: posX_(x), posY_(y), z_(z), char_(ch), font_(font), scale_(scale), color_(color), shader_(shader), type_(RQ_CHAR) {
	texture_ = font->getTexture();
}

RenderQueueItem::RenderQueueItem(int x, int y, int z, int width, int height, SpriteSheet* sheet, int index) 
    : posX_(x), posY_(y), z_(z), width_(width), height_(height), sheet_(sheet), index_(index), type_(RQ_SPRITE) {
    shader_ = sheet->getShader();
    texture_ = sheet->getTexture();
}

void RenderQueueItem::draw(Renderer* renderer) {
    switch (type_) {
    case RQ_SPRITE:
        sheet_->draw(posX_, posY_, z_, index_, width_, height_);
        break;
    case RQ_QUAD:
        renderer->drawQuadFull(posX_, posY_, z_, width_, height_, shader_, texture_);
        break;
	case RQ_CHAR:
		renderer->drawCharacter(posX_, posY_, z_, char_, font_, scale_, color_);
		break;
	case RQ_TEXT:
		renderer->drawText(posX_, posY_, z_, text_, font_, scale_, color_);
		break;
    case RQ_QUADTILING:
        renderer->drawQuadTiling(posX_, posY_, z_, width_, height_, shader_, texture_);
        break;
    case RQ_QUADBORDERED:
        renderer->drawQuadBordered(posX_, posY_, z_, width_, height_, shader_, texture_, border_);
        break;
    default:
        break;
    }
}

