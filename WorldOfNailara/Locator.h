#pragma once

#include <typeindex>
#include <unordered_map>
#include <boost\any.hpp>

#include "Library.h"

class Display;
class Logger;
class Input;

// Class to locate and serve various useful class instances
class Locator
{
private:
    static Display *display_;
    static Logger *logger_;
    static Input *input_;

	static std::unordered_map<std::type_index, LibraryGeneric*> libs_;

public:
	// Returns a library of the templated type if it exists. Otherwise it will return null.
	template <typename T> static LibraryGeneric* getLibrary() {
		if (libs_.count(std::type_index(typeid(T)))) {
			return libs_[std::type_index(typeid(T))];
		}
		else {
			Locator::getLogger()->log(SEV_FATAL, "Library not found.");
			return NULL;
		}
	}

	template <typename T> static void provide(LibraryGeneric* service) {
		std::type_index attempted = typeid(T);
		if (attempted == service->getIndex()) {
			libs_[std::type_index(typeid(T))] = service;
		}
		else {
			Locator::getLogger()->log(SEV_HEAVY, "Incorrect type provided for NewLibrary. Providing default.");
			libs_[std::type_index(typeid(T))] = new LibraryGeneric(T());
		}
	}

    static void provide(Display *service);
	static void provide(Input *service);
    static void provide(Logger *service);

    static Display* getDisplay();
    static Input* getInput();
    static Logger* getLogger();
};