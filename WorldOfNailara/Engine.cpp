#include "Engine.h"

#include "Display.h"
#include "Area.h"
#include "Locator.h"
#include "LoggerFile.h"
#include "AreaTesting.h"
#include "Input.h"

#include "Library.h"

#include "Shader.h"
#include "Texture.h"
#include "SpriteSheet.h"
#include "Loader.h"

#include "Body.h"
#include "Bodypart.h"
#include "Font.h"

Engine::~Engine()
{
	status_ = EngineStatus::DESTRUCTING;
}

void Engine::init() {
	if (status_ != EngineStatus::NEW) {
		return;
	}
	status_ = EngineStatus::STARTUP;

	// Logger must be provided first, as many other functions require it
	Locator::provide(new LoggerFile("log.log", Logger::LEV_DEBUGGER));

	// Load subsystems
    Loader::loadGraphics();

	display_ = Locator::getDisplay();
    
	activeZone_ = new AreaTesting();
	display_->setActiveArea(activeZone_);

	Locator::getLogger()->log(SEV_INFO, "Engine initialized.");

	Renderer* r = Locator::getDisplay()->getRenderer();
	Locator::getLogger();
}


void Engine::start() {
	status_ = EngineStatus::RUNTIME;
	do {
        checkInput();
        update();
		display_->render();
	} while (1 == 1);
}

void Engine::update() {
	activeZone_->update();
}

void Engine::checkInput() {
    Locator::getInput()->read();
    
	if (Locator::getInput()->hasInput()) {
        InputEvent ie = Locator::getInput()->check();
	}
}