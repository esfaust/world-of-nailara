#pragma once
#include "PromptWindow.h"

#include <vector>
#include "Entity.h"

class PromptEntityPicker :
	public PromptWindow
{
public:
	PromptEntityPicker(std::function<void()> exitCallback, std::function<void(Entity*)> callback, std::vector<Entity*> choices);

	void onNotify(Event event, InputEvent input);
	void draw();
private:
	std::vector<Entity*> choices_;
	std::function<void(Entity*)> callback_;
	int highlighted_;

    int x_, y_;
    int height_, width_;
};

