#include "PrettyText.h"
#include "Locator.h"
#include "Display.h"
#include "Shader.h"
#include "Renderer.h"


#define RGBA_TAG "rgba=([[:d:]]{1,3}),([[:d:]]{1,3}),([[:d:]]{1,3}),([[:d:]]{1,3})"

#include <regex>

std::unordered_map<std::string, std::regex> tags;

PrettyText::PrettyText() : PrettyText("", Locator::getLibrary<Font*>()->getDefault<Font*>(), 1.0f) {
}

PrettyText::PrettyText(std::string text, Font* font, float scale) : raw_(text), font_(font), scale_(scale) {
	int height = Locator::getDisplay()->getRenderer()->getHeight();
	int width = Locator::getDisplay()->getRenderer()->getWidth();

	constrain(width, height);
}

PrettyText::PrettyText(std::string text, Font* font, int xConstraint, int yConstraint, float scale) : raw_(text), font_(font), scale_(scale), xConstraint_(xConstraint), yConstraint_(yConstraint) {
	constrain(xConstraint, yConstraint);
}

PrettyTag* PrettyText::parseToken(PrettyTagType key, std::string token) {
	switch (key) {
		case PrettyTagType::RGBA:
		{
			std::regex regex = std::regex(RGBA_TAG);
			std::smatch m;
			if (std::regex_match(token, m, regex)) {
				float red = std::stof(m[1]) / 255.0f;
				float green = std::stof(m[2]) / 255.0f;
				float blue = std::stof(m[3]) / 255.0f;
				float alpha = std::stof(m[4]) / 255.0f;
				return new PrettyTagColor(Color(red, green, blue, alpha));
			}
			else {
				Locator::getLogger()->log(SEV_LIGHT, "Text tag could not be read: '"+token+"'");
				return  new PrettyTagColor(Color(1.0f, 1.0f, 1.0f));
			}

			break;
		}
		default:
			Locator::getLogger()->log(SEV_LIGHT, "Text tag could not be read: '" + token + "'");
			return new PrettyTagColor(Color(1.0f, 1.0f, 1.0f));
	}
}

std::vector<PrettyWord> PrettyText::parse(std::string text, Font* font) {
	std::vector<PrettyCharacter> parsedCharacters; // Store character by character of PrettyCharacters that have all effects stored
	std::vector<PrettyWord> parsedWords; // Then at the very end, cut them into words

	std::unordered_map<std::string, PrettyTagType> types = { {"rgba", PrettyTagType::RGBA} };

	std::vector<PrettyTag*> activeTags; // A list of all tags to apply to the current character

	bool tokenize = false; // Whether or not the incoming characters are used to construct a tag
	std::string token; // The current tag being constructed


    // Cycle through all characters one by one, reading tags into the token object to know what to apply to future characters.
	// If the activeTags list is not empty by the end, throw an error so the user knows there might be something incorrect.
	for (auto c : text) {
		if (tokenize) {

			if (c == '>') {
				// End of a token, parse it to know what to do
				tokenize = false;
				
				std::string key = token.substr(0, token.find('='));
				if (key[0] == '/') {
					key = token.substr(1, key.length());
				}
				if (types.count(key) != 0) {
					Locator::getLogger()->log(SEV_INFO, "Key: '" + key + "', Token: '" + token + "'");
					PrettyTag* tag = parseToken(types[key], token);
					activeTags.push_back(tag);
				}

				token = "";
			}
			else {
				// Construct token
				token = token + c;
			}
		}
		else {
			if (c == '<') {
				// Start tokenizing
				tokenize = true;
			}
			else {
				PrettyCharacter ch = PrettyCharacter(font, c);
				// TODO: Apply all active tags
				for (auto tag : activeTags) {
					tag->apply(ch);
				}
				//col->apply(ch);
				parsedCharacters.push_back(ch);
			}
		}
	}
	characters_ = parsedCharacters;

	//delete red;
	//delete green;
	//delete blue;

	return parsedWords;
}

void PrettyText::queue(int x, int y) {
	int adjustX = 0, adjustY = 0, lineHeight = 0;
	for (auto ch : characters_) {
		ch.queue(x + adjustX, y + adjustY);

		adjustX += ch.advance_;
		if (constrained_) {
			if (adjustX >= xConstraint_) {
				adjustY += 50;
				adjustX = 0;
			}
		}
	}
}

void PrettyText::constrain(int xConstraint, int yConstraint) {
	parse(raw_, font_);
	free(); // Free any old constraints

	xConstraint_ = xConstraint;
	yConstraint_ = yConstraint;

	std::string s = raw_;
	std::string delimiter = " ";
	std::vector<std::string> words;

	size_t pos = 0;
	std::string token;
	while ((pos = s.find(delimiter)) != std::string::npos) {
		token = s.substr(0, pos);
		words.push_back(token);
		s.erase(0, pos + delimiter.length());
	}

	int wordCount = 0, lineCount = 0;
	int currentWidth = 0, currentHeight = 0, lineHeight = 0;
	for (auto word : words) {
		PrettyWord pword = PrettyWord(word + " ", font_, scale_);

		if (pword.width() > xConstraint_ ||
			pword.height() > yConstraint_) {
			Locator::getLogger()->log(SEV_MEDIUM, "Text constraints too small to fit word '" + word + " '");
			continue; // Skip to next word
		}

		words_.push_back(pword);

		currentWidth = currentWidth + pword.width();
		lineHeight = std::max(currentHeight, pword.height());
		++wordCount;

		if (currentWidth > xConstraint_) {
			lineWordCount_.push_back(wordCount);
			currentWidth = 0;
			currentHeight = currentHeight + lineHeight;
			++lineCount;
			if (currentHeight + pword.height() > yConstraint_) {
				pages_.push_back(lineCount);
				currentHeight = 0;
			}
		}
	}

	if (lineWordCount_.size() == 0) {
		lineWordCount_.push_back(1000);
	}
	constrained_ = true;
}


void PrettyText::free() {
	words_.clear();
	lineWordCount_.clear();
	pages_.clear();
	constrained_ = false;
}


PrettyWord::PrettyWord(std::string text, Font* font, float scale) : raw_(text), font_(font), scale_(scale) {
	std::string::const_iterator c;
	int width = 0, maxHeight = 0;

	for (auto c : text) {
		PrettyCharacter ch(font, c);

		width += ch.advance_;
		maxHeight = std::max(maxHeight, ch.size_.y);
	}

	width_ = width;
	height_ = maxHeight;
}

PrettyWord::PrettyWord(std::vector<PrettyCharacter> characters) : characters_(characters) {
	int width = 0, maxHeight = 0;
	for (auto ch : characters_) {
		width += ch.advance_;
		maxHeight = std::max(maxHeight, ch.size_.y);
	}

	width_ = width;
	height_ = maxHeight;
}


void PrettyWord::queue(int x, int y) {

	Locator::getDisplay()->getRenderer()->queueText(x, y, GraphicalLayering::LAYER_PROMPT_TEXT, raw_, Locator::getLibrary<Font*>()->get<Font*>("arial-bold"), 1.0f, Color(1.0f, 1.0f, 1.0f), Locator::getLibrary<Shader*>()->get<Shader*>("text"));

}