#include "LoggerFile.h"

#include <fstream>
#include <iostream>

#include "Common.h"

LoggerFile::LoggerFile(std::string fn, LoggingLevel lv)
{
	level_ = lv;
	filename_ = fn;

	std::ofstream file;
	file.open(filename_);

	if (!file.good()) {
		exit(EXIT_FAILURE);
	}

	file.close();
	log(SEV_INFO, "Logger initialized.");
}


LoggerFile::~LoggerFile()
{
}


void LoggerFile::log(LogSeverity severity, std::string msg) {
	if ( level_ == LoggingLevel::LEV_PRODUCTION) {
		if (severity >= LogSeverity::SEV_FATAL)
			 log_to_file(severity, msg);
	}
	else if ( level_ == LoggingLevel::LEV_DEVELOPER) {
		if (severity >= LogSeverity::SEV_ERROR)
			 log_to_file(severity, msg);
	}
	else if ( level_ == LoggingLevel::LEV_DEBUGGER) {
		 log_to_file(severity, msg);
	}
}


void LoggerFile::log_to_file(LogSeverity severity, std::string msg) {
	std::ofstream file;
	file.open(filename_, std::ofstream::app);

	char buffer[16];
	sprintf_s(buffer, "%07d", logNumber_);
	++logNumber_;

	//Also log to console
	if(logToConsole_)
		std::cout << buffer << " " << now() << " " << severityText[severity] << ": '" << msg.c_str() << "'\n";

	//Log and close the file, so if we crash the log is still there
	file << buffer << " " << now() << " " << severityText[severity] << ": '" << msg.c_str() << "'\n";
	file.close();
}


std::string LoggerFile::now() {
	return Common::now_str();
}