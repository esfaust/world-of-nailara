#pragma once
#include "PromptWindow.h"
class PromptInformation :
	public PromptWindow
{
public:
	PromptInformation(std::function<void()> exitCallback, std::string text);
	~PromptInformation();

	void draw();
	void onNotify(Event event, InputEvent input);
protected:
	std::string text_;
private:
    int x_, y_;
    int height_, width_;
};

