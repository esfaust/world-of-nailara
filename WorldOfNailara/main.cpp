﻿#include "Display.h"
#include "Engine.h"

#include "Exception.h"

#include <iostream>

int main()
{

	try {
		Engine::getInstance().init();
		Engine::getInstance().start();
	}
	catch (Exception e) {

	}

	return 0;
}