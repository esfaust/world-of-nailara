#pragma once

#include <string>
#include <vector>
#include <algorithm>

#include "Locator.h"
#include "Logger.h"
#include "PrettyCharacter.h"

class Shader;


enum class PrettyTagType {
	UNDEFINED,
	RGBA
};

class PrettyTag {

public:
	virtual void apply(PrettyCharacter &ch) =0;

	void flag(std::string key, bool flag) { flags_[key] = flag; }
protected:
	Shader* shader_;
	Font* font_;
	Texture* texture_;
	
	PrettyTagType type_ = PrettyTagType::UNDEFINED;
	
	std::unordered_map<std::string, bool> flags_; // Such as "wiggle" or "wave"
	std::unordered_map<std::string, float> values_; // Such as "wiggleSpeed" or rgb
};

class PrettyTagColor : public PrettyTag{
public:
	PrettyTagColor(Color color) : color_(color) {
		type_ = PrettyTagType::RGBA;
	}

	void apply(PrettyCharacter &ch) {

		ch.color_ = color_;
	}

private:
	Color color_;
};

class PrettyTagScale : public PrettyTag {
public:
	void apply(PrettyCharacter* ch) {
		ch->size_ = glm::ivec2(flags_["scaleX"], flags_["scaleY"]);
	}
};

class PrettyWord {
public:
	PrettyWord(std::string text, Font* font, float scale = 1.0f);
	PrettyWord(std::vector<PrettyCharacter> characters);

	void queue(int x, int y);

	int width() { return width_; }
	int height() { return height_; }
private:
	std::string raw_;
	int width_, height_;
	Font* font_;
	float scale_;

	std::vector<PrettyCharacter> characters_;
};

class PrettyText {
public:
	PrettyText();
	PrettyText(std::string text, Font* font, float scale = 1.0f);
	PrettyText(std::string text, Font* font, int xConstraint, int yConstraint, float scale = 1.0f);

	void queue(int x, int y); // Queue for rendering
	void constrain(int xConstraint, int yConstraint); // Constrains the text to a certain size and automatically paginates
	void free(); // Frees the text from any constraints

	
private:

	std::vector<PrettyWord> parse(std::string, Font* font);
	PrettyTag* parseToken(PrettyTagType key, std::string token);

	std::vector<PrettyCharacter> characters_;


	Font* font_; // Font to display with
	float scale_; // Font or text scale
	std::string raw_; // The raw text to be displayed
	std::vector<PrettyWord> words_; // Pretty words that exist in the raw text - does not include spaces.
	std::vector<int> lineWordCount_; // Used when constrained, the number of words to display on a line.
	std::vector<int> pages_; // Used when constrained, the number of lines per page.
	int xConstraint_ =0, yConstraint_ =0; // Constraining bounds
	bool constrained_ = false; // Whether or not to display constrained
};

