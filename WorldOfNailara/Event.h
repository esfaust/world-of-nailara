#pragma once

enum EventType {
	EVENT_KEYBOARD,
	EVENT_MOUSE,
	EV_ENTITY_MOVEMENT,
	PROMPT_DESTROYED
};

// This class is used to differentiate between event types for observers
class Event
{
public:
	Event(EventType type) : type_(type) {}

	//The event type
	EventType type_;
};

