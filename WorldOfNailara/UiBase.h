#pragma once

#include "ViewPort.h"
#include "Renderer.h"

class UiBase
{
public:
	UiBase(Renderer* renderer, ViewPort* viewPort, bool asciiMode=false) : renderer_(renderer), viewPort_(viewPort), asciiMode_(asciiMode) {}
	virtual void draw()=0;

protected:
	Renderer* renderer_;
	ViewPort* viewPort_;
	bool asciiMode_ = false;
};

