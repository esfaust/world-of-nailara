#include "AiDancing.h"

#include "Actor.h"


int AiDancing::takeTurn() {
	if (AP_ < 0) {
		return AP_;
	}

	static bool stage = false;

	if (stage) {
		owner_->shift(1, 0);
	}
	else {
		owner_->shift(-1, 0);
	}

	stage = !stage;

	AP_ -= 200;
	return AP_;
}

int AiDancing::regenerateAP() {
	AP_ += 50;
	return AP_;
}