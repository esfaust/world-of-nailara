#pragma once

#include <string>

enum LogSeverity {
	SEV_INFO, // Just informative to help show how the program flows
	SEV_HEAVY, // An error occurred that doesn't immediately cause crash, but will likely in the future
	SEV_MEDIUM, // An error that will ruin a part of gameplay, but the program can continue
	SEV_LIGHT, // An error that is excised - something will be missing, but otherwise working
	SEV_WARNING,
	SEV_ERROR,
	SEV_FATAL // The game is about to crash.
};

/*
Interface class to log things
*/
class Logger
{
public:
	enum LoggingLevel {
		LEV_DEBUGGER, // All
		LEV_DEVELOPER, // Warnings, errors, and fatals.
		LEV_PRODUCTION // Errors and fatals.
	};
	virtual void log(LogSeverity severity, std::string msg) =0;
};

// Null logger that does nothing, for if the locator ever is not served a logger
// Alternatively, if no logging should be done, use this
class NullLogger : public Logger
{
public:
	NullLogger() {};
	void log(LogSeverity severity, std::string msg) {};
};