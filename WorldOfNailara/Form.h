#pragma once

#include "Body.h"
#include "BlockingFlags.h"
#include "BodypartConnection.h"

class Body;

class Form : public Attributes {
public:
    Form(BodypartConnection base) : base_(base){};
    Blocking::Set blocking_ = Blocking::S_None; // What blocks movement of this form, if any

    BodypartConnection getBase();
private:
    BodypartConnection base_; // The first bodypart to add on to.
};

/*
A FormDescriptor is given a form, and generates a description and features based on that form layout. Thus, two orcs that have the same layout (humanoid + tusks) can have different features.
A descriptor set might describe eye color, skin color, height, and other visual features.
*/
class FormDescriptor {
public:
    FormDescriptor(Form* layout, Body* parent) : layout_(layout), parent_(parent) {};

    std::string getDescription(bool longDesc = false);

private:
    std::string applyAttributesToTemplate(std::string descTemplate);
    
    Body* parent_;
    Form* layout_;
    std::string shortDescription_; // Such as "A human wielding a longbow."
    bool descriptionGenerated_ = false;
    std::string longDescription_; // Such as "Ted is a 5'11" tall male human with dark brown hair. He has blue eyes and..."
};