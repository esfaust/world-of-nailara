#pragma once
#include "Targeting.h"

#include "PromptEntityPicker.h"

class Entity;

class TargetingAdjacent :
	public Targeting
{
public:
	TargetingAdjacent(Entity* caller);
	~TargetingAdjacent();
	void target();
	bool hasValidTargets();
	void applyEffect(Effect* effect);
	void targetAndApply(Effect* effect);
private:
	void promptHandler(Entity* chosenTarget);
    void promptExitHandler();

	Entity* tempTarget_;
	Entity* caller_;
	Effect* tempEffect_;
};

