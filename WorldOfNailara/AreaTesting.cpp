#include "AreaTesting.h"

#include "Locator.h"
#include "Logger.h"
#include "Actor.h"
#include "AiDancing.h"
#include "Display.h"

AreaTesting::AreaTesting() : Area(15, 10)
{
    for (unsigned int i = 0; i < width_; i++) {
        Tile t = Tile(Locator::getLibrary<Sprite*>()->get<Sprite*>("wall"), i, 0, Blocking::S_Wall);
        tiles_[i][0] = t;
        t = Tile(Locator::getLibrary<Sprite*>()->get<Sprite*>("wall"), i, height_ - 1, Blocking::S_Wall);
        tiles_[i][height_ - 1] = t;
    }

    for (unsigned int i = 0; i < height_; i++) {
		Tile t = Tile(Locator::getLibrary<Sprite*>()->get<Sprite*>("wall"), 0, i, Blocking::S_Wall);
		tiles_[0][i] = t;
		t = Tile(Locator::getLibrary<Sprite*>()->get<Sprite*>("wall"), width_ - 1, i, Blocking::S_Wall);
		tiles_[width_-1][i] = t;
	}
    for (int i = 0; i < 8; i++) {
		if (i == 3 || i == 5) {
			Entity* window = new Entity("window", Locator::getLibrary<Sprite*>()->get<Sprite*>("window"), Location(this, 11, i + 1), Blocking::S_Window);
			window->attr("desc", "You can see your own reflection in the window. You quickly look away, horrified.");
			addEntity(window);
			continue;
		}
		if (i == 7) {
			Entity* door = new Entity("door", Locator::Locator::getLibrary<Sprite*>()->get<Sprite*>("door"), Location(this, 11, i + 1), Blocking::S_Door);
			door->attr("desc", "Someone has propped up a cardboard door, colored with crayons.");
			addEntity(door);
			continue;
		}
		tiles_[11][i+1] = Tile(Locator::getLibrary<Sprite*>()->get<Sprite*>("wall"), 11, i+1, Blocking::S_Wall);
	}

	for (int i = 0; i < 3; i++) {
		Entity* smoke = new Entity("smoke", Locator::Locator::getLibrary<Sprite*>()->get<Sprite*>("smoke"), Location(this, 2, i + 1), Blocking::S_None);
		smoke->attr("desc", "A perfectly spherical orb of smoke. It smells like burnt steak.");
		addEntity(smoke);
		if (i == 2) {
			Entity* smoke2 = new Entity("smoke", Locator::Locator::getLibrary<Sprite*>()->get<Sprite*>("smoke"), Location(this, 1, i + 1), Blocking::S_None);
			smoke2->attr("desc", "A perfectly spherical orb of smoke. It smells like burnt steak.");
			addEntity(smoke2);
		}
	}
    
	for (int i = 0; i < 5; i++) {
		if (i != 3) {
			Entity* smoke = new Entity("smoke", Locator::Locator::getLibrary<Sprite*>()->get<Sprite*>("smoke"), Location(this, i + 1, 7), Blocking::S_None);
			smoke->attr("desc", "A perfectly spherical orb of smoke. It smells like burnt steak.");
			addEntity(smoke);
		}
		if (i == 4) {
			Entity* smoke = new Entity("smoke", Locator::Locator::getLibrary<Sprite*>()->get<Sprite*>("smoke"), Location(this, i + 1, 8), Blocking::S_None);
			smoke->attr("desc", "A perfectly spherical orb of smoke. It smells like burnt steak.");
			addEntity(smoke);
		}
	}

    
	addEntity(new Entity("torch", Locator::Locator::getLibrary<Sprite*>()->get<Sprite*>("torch"), Location(this, 3, 2), Blocking::S_Creature));


	Actor* player = new Actor("player", Locator::Locator::getLibrary<Sprite*>()->get<Sprite*>("player"), Location(this, 1, 2));
	player->attr("desc", "Despite everything, it's still you");
	addActor(player);

	Actor* dancer = new Actor("Kona", Locator::Locator::Locator::getLibrary<Sprite*>()->get<Sprite*>("ally"), Location(this, 5, 5));
	dancer->attr("desc", "It's your friend Kona. She's dancing. She's not very good at it, but she looks like she's having fun.");
	dancer->setPersonality(new AiDancing(dancer));
	addActor(dancer);


    //focalPoint_.setTracking(player);
    //Locator::getDisplay()->setFocalPoint(focalPoint_);
    Locator::getDisplay()->view()->focalTracking(player);
	
}


AreaTesting::~AreaTesting()
{
}
