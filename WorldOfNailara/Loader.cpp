#include "Loader.h"

#include "Shader.h"
#include "Texture.h"
#include "Locator.h"
#include "SpriteSheet.h"
#include "Display.h"

#include "RenderItem.h"
#include "Drawable.h"

void Loader::loadGraphics() {
	// Create new graphical display
	Locator::provide(new Display("World of Nailara", 80, 25, 8, 4));

	// Create libraries for the graphics to use
	Shader* errorShader = new Shader("error.vs", "error.fs");
	Texture* errorTexture = new Texture("error.png");
	SpriteSheet* errorSheet = new SpriteSheet(errorTexture, errorShader, 32, 32);

	Locator::provide<Shader*>(new LibraryGeneric(errorShader));
	Locator::provide<Texture*>(new LibraryGeneric(errorTexture));
	Locator::provide<SpriteSheet*>(new LibraryGeneric(errorSheet));
	Locator::provide<Sprite*>(new LibraryGeneric(errorSheet->generateSprite(0)));

	// CRITICAL LOADS //
	Shader* basicnews = Locator::getLibrary<Shader*>()->add("basic", new Shader("basic.vs", "basic.fs"));
	Texture* debugt = Locator::getLibrary<Texture*>()->add("debug", new Texture("Media\\debug.png"));
	Texture* errort = Locator::getLibrary<Texture*>()->add("error", new Texture("Media\\error.png"));
	SpriteSheet* debugsheet = Locator::getLibrary<SpriteSheet*>()->add("debug-sheet", new SpriteSheet(debugt, basicnews, 64, 64));
	Locator::getLibrary<Sprite*>()->add("debug-sprite0", debugsheet->generateSprite(0));
	Locator::getLibrary<Sprite*>()->add("debug-sprite1", debugsheet->generateSprite(1));
	Locator::getLibrary<Sprite*>()->add("debug-sprite2", debugsheet->generateSprite(2));

	Locator::getLibrary<Shader*>()->add("text", new Shader("text.vs", "text.fs"));

    Texture* tiles = new Texture("Media\\tiles.png");
    Texture* bg = new Texture("Media\\background.png");
    Texture* bg2 = new Texture("Media\\background2.png");
    Texture* borh = new Texture("Media\\borderhorizontal.png");

    Locator::getLibrary<Texture*>()->add("bg", bg);
    Locator::getLibrary<Texture*>()->add("bg2", bg2);
    Locator::getLibrary<Texture*>()->add("borderh", borh);
    Locator::getLibrary<Texture*>()->add("tiles", tiles);
	
    Locator::getLibrary<SpriteSheet*>()->add("tiles", new SpriteSheet(tiles, Locator::getLibrary<Shader*>()->get<Shader*>("basic"), 64, 64));

    Locator::getLibrary<Sprite*>()->add("player", Locator::getLibrary<SpriteSheet*>()->get<SpriteSheet*>("tiles")->generateSprite(0));
    Locator::getLibrary<Sprite*>()->add("enemy", Locator::getLibrary<SpriteSheet*>()->get<SpriteSheet*>("tiles")->generateSprite(1));
    Locator::getLibrary<Sprite*>()->add("ally", Locator::getLibrary<SpriteSheet*>()->get<SpriteSheet*>("tiles")->generateSprite(2));
    Locator::getLibrary<Sprite*>()->add("door", Locator::getLibrary<SpriteSheet*>()->get<SpriteSheet*>("tiles")->generateSprite(5));
    Locator::getLibrary<Sprite*>()->add("window", Locator::getLibrary<SpriteSheet*>()->get<SpriteSheet*>("tiles")->generateSprite(6));
    Locator::getLibrary<Sprite*>()->add("smoke", Locator::getLibrary<SpriteSheet*>()->get<SpriteSheet*>("tiles")->generateSprite(7));
    Locator::getLibrary<Sprite*>()->add("floor", Locator::getLibrary<SpriteSheet*>()->get<SpriteSheet*>("tiles")->generateSprite(8));
    Locator::getLibrary<Sprite*>()->add("wall", Locator::getLibrary<SpriteSheet*>()->get<SpriteSheet*>("tiles")->generateSprite(9));

    Locator::getLibrary<SpriteSheet*>()->add("kona", new SpriteSheet(new Texture("Media\\kona.png"), Locator::getLibrary<Shader*>()->get<Shader*>("basic"), 512, 512));
    Locator::getLibrary<Sprite*>()->add("kona", Locator::getLibrary<SpriteSheet*>()->get<SpriteSheet*>("kona")->generateSprite(0));

	loadFonts();

	ExampleDrawable* ed = new ExampleDrawable(Locator::getLibrary<Sprite*>()->get<Sprite*>("player"), basicnews);
	ExampleDrawable* ed2 = new ExampleDrawable(Locator::getLibrary<Sprite*>()->get<Sprite*>("player"), basicnews);
	ExampleDrawable* ed3 = new ExampleDrawable(Locator::getLibrary<Sprite*>()->get<Sprite*>("player"), basicnews);
	ExampleDrawable* ed4 = new ExampleDrawable(Locator::getLibrary<Sprite*>()->get<Sprite*>("player"), basicnews);
	ExampleDrawable* ed5 = new ExampleDrawable(Locator::getLibrary<Sprite*>()->get<Sprite*>("player"), basicnews);
	ExampleDrawable* ed6 = new ExampleDrawable(Locator::getLibrary<Sprite*>()->get<Sprite*>("player"), basicnews);

	ed->queue(300, 300, 1);
	ed2->queue(325, 310, 3);
	ed3->queue(350, 320, 2);

	ed4->queue(400, 320, 10);
	ed5->queue(400, 340, 9);
	ed6->queue(400, 360, 8);

	Renderer* r = Locator::getDisplay()->getRenderer();
	Locator::getLogger();
}


#include "Font.h"

void Loader::loadFonts() {
	Font* defaultFont = new Font("arial-bold");
	Locator::provide<Font*>(new LibraryGeneric(defaultFont));

    Locator::getLibrary<Font*>()->add("arial-bold", defaultFont);
}

#include "Body.h"

void Loader::loadBodies() {

}