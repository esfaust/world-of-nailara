#pragma once

#include <string>

class Texture;
class Shader;
class Renderer;
class Sprite;

class SpriteSheet
{
public:
    SpriteSheet(Texture* texture, Shader* shader, int spriteWidth, int spriteHeight);
    SpriteSheet(std::string textureFile, std::string vertexShaderFile, std::string fragmentShaderFile, int spriteWidth, int spriteHeight);
    void draw(int x, int y, int z, int index);
    void draw(int x, int y, int z, int index, int width, int height);
    void queue(int spriteIndex, int posX, int posY, int z);
    void queue(int spriteIndex, int posX, int posY, int z, int width, int height);
    Sprite* generateSprite(int spriteIndex);

    int getSpriteHeight();
    int getSpriteWidth();
    Texture* getTexture();
    Shader* getShader();

private:
    Texture* texture_;
    Shader *shader_;
    Renderer *renderer_;

    int sheetWidth_ = 128, sheetHeight_ = 32;
    int spriteWidth_ = 32, spriteHeight_ = 32;
	float tw_ = 1.0f;
	float th_ = 1.0f;
    int numPerRow_ = 1;
};
