#pragma once

class Effect;
class Targeting;


/*
 The ability class is used by actors to activate instantaneous effects - shooting a fireball, healing themselves, adding a timed buff/debuff to a target
 All abilities must have an effect that happens to a target when used
*/
class Ability
{
public:	
	//Initializes a new Ability, using an effect pointer from the library, and a targeting style from the library
	Ability(Effect* effect, Targeting* targeting);
	//Destroys the ability - does not deallocate the effect and targeting style
	~Ability();

	//Returns whether or not the ability is valid to use - are there any targets to target?
	bool isValid();
	//Uses the ability - this includes choosing a target
	void use();

protected:
	Effect* effect_; // Effect to apply to chosen target. Not owned by this class.
	Targeting* targeting_; // Style of targeting. Not owned by this class
};

