#include "Ability.h"

#include <exception>

#include "Exception.h"
#include "Targeting.h"
#include "Locator.h"

Ability::Ability(Effect* effect, Targeting* targeting) : effect_(effect), targeting_(targeting)
{
}


Ability::~Ability()
{
	if(effect_) delete effect_;
	if (targeting_) delete targeting_;
}

void Ability::use() {
	if (targeting_->hasValidTargets()) {
		targeting_->targetAndApply(effect_);
		Locator::getLogger()->log(SEV_INFO, "ability use");
		//targeting_->applyEffect(effect_);
	}
}