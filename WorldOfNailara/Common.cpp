#include "Common.h"

#include <iostream>
#include <ctime>
#include <time.h>
#include <iomanip>
#include <chrono>

#include <windows.h>

#include <sstream>

#ifdef WIN32
#define localtime_r(_Time, _Tm) localtime_s(_Tm, _Time)

std::string Common::now_str() {
	tm localTime;
	std::chrono::system_clock::time_point t = std::chrono::system_clock::now();
	time_t now = std::chrono::system_clock::to_time_t(t);
	localtime_r(&now, &localTime);

	const std::chrono::duration<double> tse = t.time_since_epoch();
	std::chrono::seconds::rep milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(tse).count() % 1000;

	std::stringstream buf;
	buf << (1900 + localTime.tm_year) << '-'
		<< std::setfill('0') << std::setw(2) << (localTime.tm_mon + 1) << '-'
		<< std::setfill('0') << std::setw(2) << localTime.tm_mday << ' '
		<< std::setfill('0') << std::setw(2) << localTime.tm_hour << ':'
		<< std::setfill('0') << std::setw(2) << localTime.tm_min << ':'
		<< std::setfill('0') << std::setw(2) << localTime.tm_sec << '.'
		<< std::setfill('0') << std::setw(3) << milliseconds;

	return buf.str();
}

unsigned long Common::time_ms() {

	unsigned long milliseconds_since_epoch =
		std::chrono::system_clock::now().time_since_epoch() /
		std::chrono::milliseconds(1);

	return milliseconds_since_epoch;
}

#endif


#include <GLFW\glfw3.h>
double Common::time() {
	return glfwGetTime();
}