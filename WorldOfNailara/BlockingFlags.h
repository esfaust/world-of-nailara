#pragma once

/*
Blocking flags using bit masks.
An entity has a blocking flag that determines what they block

IE:
An orc walks up to a door. The orc blocks Creature, Projectiles. 00110
The door blocks Creature, Projectiles, and Gas. 01110
We && the two flags together, resulting in: 00110. It's greater than 0, so they cannot pass each other.

A ghost floats up to a door. Ghosts block only Ethereal. 10000
The door blocks Creature, Projectiles, and Gas. 01110
We && the two flags together, resulting in: 00000. They can pass each other.

Examples:
Portcullis - 00110, vs Creatures and Projectiles
Fortifications - 00010, vs Creatures
Sigil of Banishment - 10000, vs ethereal
*/
namespace Blocking {
	enum Flag {
		F_None = 0, //Nothing blocks with it
		F_Creature = 1u << 1, // So creatures cannot move through each other
		F_Projectiles = 1u << 2, // So you can shoot through arrow slats, but not move through them
		F_Gas = 1u << 3, // Air tight containers, other gas, etc
		F_Ethereal = 1u << 4, // A thing that blocks other things on the ethereal plane
		F_Furniture = 1u << 5, // Chairs dont block much other than other chairs
		F_Vision = 1u << 6,
		F_Walls = 1u << 7
	};

	enum Set {
		S_None = F_None,
		S_Wall = F_Walls | F_Creature | F_Vision,
		S_Creature = F_Creature,
		S_Window = F_Creature,
		S_Door = F_None
	};
}

// A static helper class used to perform calculations on bitmask flags
class FlagCalculation {
public:
	static bool isBlockedBy(Blocking::Flag A, Blocking::Flag B) {
		return !!(A & B);
	}
	static bool isBlockedBy(Blocking::Set A, Blocking::Set B) {
		return !!(A & B);
	}
};
