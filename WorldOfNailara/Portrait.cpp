#include "Portrait.h"

#include "Locator.h"
#include "Logger.h"
#include "Sprite.h"

Portrait::Portrait(Sprite* sprite) : default_(sprite), currentEmotion_(sprite)
{
    add("default", sprite);
}


Portrait::~Portrait()
{
}


void Portrait::add(std::string emotion, Sprite* sprite) {
    sprites_[emotion] = sprite;
}

void Portrait::emotion(std::string emotion) {
    if (sprites_.count(emotion) <= 0) {
        if (default_ == NULL) {
            Locator::getLogger()->log(SEV_HEAVY, "Default portrait sprite not found.");
        }
    }
    else {
        currentEmotion_ = sprites_[emotion];
    }
}

int Portrait::getHeight() {
    return currentEmotion_->getHeight();
}

int Portrait::getWidth() {
    return currentEmotion_->getWidth();
}

void Portrait::queue(int x, int y, int z) {
    currentEmotion_->queue(x, y, z);
}

void Portrait::queue(int x, int y, int z, int width, int height) {
    currentEmotion_->queue(x, y, z, width, height);
}