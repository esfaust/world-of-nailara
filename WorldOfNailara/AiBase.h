#pragma once

#include <memory>

class Actor;

/*
 AI base class from which other kinds of AI will be derived.
 This class handles how the creature acts, when they move, and most of the logic of enemies
 These classes can also be the area where a player is prompted for input
*/
class AiBase
{
public:
	// A base AI must know who owns it so it knows who to move and modify
	AiBase(Actor* owner) : owner_(owner) {}
	// Virtual function called when its the owner's turn, when they have AP
	virtual int takeTurn() = 0;
	// Add the actor's speed to the available AP
	virtual int regenerateAP() = 0;

protected:
	// The actor's store of energy to perform actions
	// Once the creature's AP is below 0, their turn is over
	int AP_;
	// The actor who is being modified by this personality
	Actor* owner_;
};

// A null AI used when the creature can never take actions. They will be skipped every time
class AiNull : public AiBase {
public:
	AiNull() : AiBase(NULL) {}
	int takeTurn() { return -1; }
	int regenerateAP() { return -1; }
};