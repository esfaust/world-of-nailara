#pragma once

#include <string>
#include <unordered_map>
#include <boost\any.hpp>

#include "Locator.h"
#include "Logger.h"


class Attributes {
protected:
    std::unordered_map<std::string, boost::any> attributes_;
public:

    template <typename T> T attr(std::string name) {
        if (attributes_.count(name)) {
            try
            {
                T result = boost::any_cast<T>(attributes_[name]);
                return result;
            }
            catch (const boost::bad_any_cast &)
            {
                Locator::getLogger()->log(SEV_FATAL, "Bad attribute casting for key='" + name + "'");
                return T();
            }
        }
        else {
            Locator::getLogger()->log(SEV_FATAL, "Attribute not present for key='" + name + "'");
            return T();
        }
    }

    // Strings decay into const char*, this specialization does the conversion back. attr<std::String>
    template <> std::string attr<std::string>(std::string name) {
        if (attributes_.count(name)) {
            try
            {
                const char* ch = boost::any_cast<char const *>(attributes_[name]);
                std::string str = std::string(ch);
                return str;
            }
            catch (const boost::bad_any_cast &)
            {
                Locator::getLogger()->log(SEV_FATAL, "Bad attribute string casting for key='" + name + "'");
                return "";
            }

        }
        else {
            Locator::getLogger()->log(SEV_FATAL, "Attribute not present for key='" + name + "'");
            return "";
        }
    }

    // Booleans are stored as integers in these attributes so that when multiple things grant an attribute, it won't disappear when one sets it to false/removes itself
    // Will return false if: the key doesn't exist, or the value is 0
    template <> bool attr<bool>(std::string name) {
        if (attributes_.count(name)) {
            try
            {
                int val = boost::any_cast<int>(attributes_[name]);
                return val;
            }
            catch (const boost::bad_any_cast &)
            {
                Locator::getLogger()->log(SEV_FATAL, "Bad attribute bool casting for key='" + name + "'");
                return false;
            }

        }
        else {
            return false;
        }
    }

    bool isCharPtr(const boost::any &operand) {
        try {
            boost::any_cast<char const *>(operand);
            return true;
        }
        catch (const boost::bad_any_cast &) {
            return false;
        }
    }

    template <typename T> void attr(std::string name, T val) {
        attributes_[name] = val;
    }

    // Increases the bool value, and destroys it if there 
    template <> void attr<bool>(std::string name, bool val) {
        int newVal = 0;
        if (attributes_.count(name)) {
            try {
                int current = boost::any_cast<int>(attributes_[name]);
                if (val)
                    newVal = current++;
                else
                    newVal = current--;
            }
            catch (const boost::bad_any_cast &) { return Locator::getLogger()->log(SEV_FATAL, "Bad attribute bool setting casting for key='" + name + "'"); }
        }
        else {
            newVal = val;
            if (!newVal) return; // If the key doesnt exist, and the value is false, dont do anything
        }

        if (newVal) {
            attributes_[name] = newVal;
        }
        else {
            attributes_.erase(name);
        }
    }

    bool attrExists(std::string name) { return attributes_.count(name); }
};